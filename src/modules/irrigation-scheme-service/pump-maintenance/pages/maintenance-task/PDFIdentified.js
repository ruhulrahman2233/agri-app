import i18n from '@/i18n'
import Store from '@/store'
// import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'
// import { dateFormat } from '@/Utils/fliter'
const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, complainID, complainBy, complainEquipment, divName, disName, upzillaName, unionName, complainEquipmentDetails, vm) => {
  try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
      const pdfContent = [
           ]
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center' })
        const complainHead = [
          [
            { text: vm.$t('irri_pump_main.complain_id'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? vm.$n(complainID) : complainID, style: 'td', alignment: 'left' }
          ],
          [
            { text: vm.$t('irri_pump_main.complained_by'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? complainBy : complainBy, style: 'td', alignment: 'left' }
          ]
        ]
        pdfContent.push({
          table: {
            headerRows: 1,
            widths: ['15%', '2%', '15%', '15%', '10%', '30%'],
            body: complainHead
          },
          layout: {
            hLineWidth: function (i, node) {
              return 0
            },
            vLineWidth: function (i, node) {
              return 0
            }
          }
        })
        const allRowsHead2 = [
          [
            { text: vm.$t('irri_pump_main.division'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: divName, style: 'td', alignment: 'left' },
            { text: vm.$t('irri_pump_main.district'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: disName, style: 'td', alignment: 'left' },
            { text: vm.$t('org_pro_upazilla.upazilla'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: upzillaName, style: 'td', alignment: 'left' },
            { text: vm.$t('irri_pump_main.union'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: unionName, style: 'td', alignment: 'left' }
          ],
          [
            { text: vm.$t('irri_pump_main.pump_id'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? complainEquipment.pump_id === undefined ? '' : vm.$n(complainEquipment.pump_id) : complainEquipment.pump_id, style: 'td', alignment: 'left' },
            { text: vm.$t('irri_pump_main.mauza_no'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? complainEquipment.mauza_no : complainEquipment.mauza_no, style: 'td', alignment: 'left' },
            { text: vm.$t('irri_pump_main.jl_no'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? vm.$n(complainEquipment.jl_no) : complainEquipment.jl_no, style: 'td', alignment: 'left' },
            { text: vm.$t('irri_pump_main.plot_no'), style: 'td', alignment: 'left' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? vm.$n(complainEquipment.plot_no) : complainEquipment.plot_no, style: 'td', alignment: 'left' }
          ]
        ]
        pdfContent.push({
          table: {
            headerRows: 1,
            widths: ['10%', '2%', '15%', '10%', '2%', '15%', '10%', '2%', '15%', '10%', '2%', '15%'],
            body: allRowsHead2
          },
          layout: {
            hLineWidth: function (i, node) {
              return 0
            },
            vLineWidth: function (i, node) {
              return 0
            }
          }
        })

        pdfContent.push({ text: vm.$t('irri_pump_main.trouble_equipment_details'), style: 'header2', alignment: 'left' })
        const detailsData = [
          [
            { text: vm.$t('irri_pump_main.name'), style: 'th', alignment: 'center' },
            { text: vm.$t('irri_pump_main.note'), style: 'th', alignment: 'center' }
          ]
        ]
        complainEquipmentDetails.map((item, index) => {
          detailsData.push(
            [
              { text: (i18n.locale === 'bn') ? item.name_bn : item.name, style: 'td', alignment: 'center' },
              { text: (i18n.locale === 'bn') ? item.note_bn : item.note, style: 'td', alignment: 'center' }
            ]
          )
        })
        pdfContent.push({
          table: {
            headerRows: 2,
            widths: ['50%', '50%'],
            body: detailsData
          }
        })

        pdfContent.push({ text: '', style: 'fertilizer', alignment: 'center' })
        const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'portrait',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
            th: {
              fontSize: 10,
              margin: [3, 3, 3, 3]
            },
            th1: {
                fontSize: 9
              },
            td: {
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            search: {
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            fertilizer: {
              margin: [0, 10, 0, 7]
            },
            fertilizerSHeader: {
                fontSize: 10,
                margin: [40, 0, 0, 0]
            },
            header: {
              fontSize: 12,
              margin: [0, 0, 0, 4]
            },
            header2: {
              fontSize: 10,
              margin: [0, 10, 0, 20]
            },
            headerPort1: {
              fontSize: 10,
              margin: [0, 20, 0, 5]
            },
            headerPort: {
              fontSize: 10,
              margin: [0, 4, 0, 15]
            },
            krishi: {
              margin: [0, 1, 0, 15],
              alignment: 'center'
            },
            header3: {
              fontSize: 9,
              margin: [0, 15, 0, 0]
            },
            address: {
              fontSize: 9,
              margin: [0, -10, 0, 0]
            },
            tableSubHead: {
              margin: [0, 5, 0, 15]
            }
          }
        }
        pdfMake.createPdf(docDefinition, null, null, null).download('identified-equipment')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
