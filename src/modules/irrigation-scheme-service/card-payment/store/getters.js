export default {
  NewApplicationRequestFind: (state) => (id) => state.NewApplicationRequests.find(NewApplicationRequest => NewApplicationRequest.id === id),
  ReissueApplicationRequestFind: (state) => (id) => state.ReissueApplicationRequests.find(ReissueApplicationRequest => ReissueApplicationRequest.id === id),
  SmartCardApproveFind: (state) => (id) => state.SmartCardApproves.find(SmartCardApprove => SmartCardApprove.id === id),
  SmartCardFind: (state) => (id) => state.SmartCards.find(SmartCard => SmartCard.id === id)
}
