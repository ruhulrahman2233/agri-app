export default {
  waterTestingRequestFind: (state) => (id) => state.waterTestingRequests.find(waterTestingRequest => waterTestingRequest.id === id)
}
