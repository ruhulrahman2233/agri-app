// farmer-water-test-request
export const waterTestingRequestList = 'farmer-water-test-application/list'
export const waterTestingRequestStore = 'farmer-water-test-application/store'
export const waterTestingRequestUpdate = 'farmer-water-test-application/update'
export const waterTestingRequestToggleStatus = 'farmer-water-test-application/toggle-status'
export const waterTestingRequestDestroy = 'farmer-water-test-application/delete'
