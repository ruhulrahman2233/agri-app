export default {
  testList: state => state.tests,
  testFind: state => (id) => state.tests.find(item => item.id === id)
}
