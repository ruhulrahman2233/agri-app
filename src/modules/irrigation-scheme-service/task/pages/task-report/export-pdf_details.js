import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'
// import { dateFormat } from '@/Utils/fliter'

const exportPdfDetails = async (baseUrl, uri, orgId, reportTitle, data, vm) => {
  try {
    Store.commit('mutateCommonProperties', {
      loading: true
    })
    if (i18n.locale === 'bn') {
      pdfMake.vfs = pdfFontsBn.pdfMake.vfs
    } else {
      pdfMake.vfs = pdfFontsEn.pdfMake.vfs
    }
    const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
    const pdfContent = [
      {
        columns: reportHeadData.reportHeadColumn, style: 'main_head'
      },
      {
        text: vm.$t('fertilizerReport.krishi_bhaban'),
        style: 'krishi',
        alignment: 'center'
      },
      { text: reportHeadData.address, style: 'address', alignment: 'center' }
    ]
    // pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center' })
    /**
     * Header start pdf
     */
     pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
    const allRowsHead = [
        [
        { text: vm.$t('irrigation_task.task'), style: 'th', alignment: 'center' },
        { text: vm.$t('irrigation_task.task_report'), style: 'th', alignment: 'center' }
      ]
    ]

    pdfContent.push({
        table: {
          headerRows: 1,
          widths: ['70%', '15%'],
          body: allRowsHead
        },
        layout: {
          hLineWidth: function (i, node) {
            return 0
          },
          vLineWidth: function (i, node) {
            return 0
          }
        }
      })
    /**
     * header end pdf
     */
     // pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
      const headList = [
        { text: vm.$t('globalTrans.sl_no'), style: 'th', alignment: 'center' },
        { text: vm.$t('globalTrans.organization'), style: 'th', alignment: 'center' },
        { text: vm.$t('globalTrans.name'), style: 'th', alignment: 'center' },
        { text: vm.$t('irrigation_task.task_id'), style: 'th', alignment: 'center' },
        { text: vm.$t('irrigation_task.task_name'), style: 'th', alignment: 'center' },
        { text: vm.$t('irrigation_task.responsible'), style: 'th', alignment: 'center' },
        { text: vm.$t('globalTrans.attachment'), style: 'th', alignment: 'center' },
        { text: vm.$t('globalTrans.schedule'), style: 'th', alignment: 'center' },
        { text: vm.$t('globalTrans.status'), style: 'th', alignment: 'center' },
        { text: vm.$t('globalTrans.attachment'), style: 'th', alignment: 'center' },
        { text: vm.$t('irrigation_task.note'), style: 'th', alignment: 'center' },
        { text: vm.$t('irrigation_task.task_calendar_date'), style: 'th', alignment: 'center' }
      ]

            const allRows = [headList]
            data.map((report, index) => {
              const newRow = [
                { text: vm.$n(index + 1), style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.org_name_bn : report.org_name, style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.name_bn : report.name, style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.task_id_bn : report.task_id, style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.task_name_bn : report.task_name, style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.assign_username : report.assign_username, style: 'td', alignment: 'center' },
                { text: '' },
                { text: (i18n.locale === 'bn') ? report.schedule : report.schedule, style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.status : report.status, style: 'td', alignment: 'center' },
                { text: '' },
                { text: (i18n.locale === 'bn') ? report.task_reports_note : report.task_reports_note, style: 'td', alignment: 'center' },
                { text: (i18n.locale === 'bn') ? report.task_date_bn : report.task_date, style: 'td', alignment: 'center' }
              ]
          allRows.push(newRow)
        })
        // const footList = [
        //   { text: vm.$t('fertilizerReport.GrandTotalMTon'), style: 'th', colSpan: 3, alignment: 'right', bold: true },
        //   {},
        //   {}
        // ]
        // fertilizerNameList.map((item, index) => {
        //   footList.push(
        //     { text: vm.$n(getTotalData(data, item.value)), style: 'th', alignment: 'right', bold: true }
        //   )
        // })
        // allRows.push(footList)

      pdfContent.push({
        table: {
          headerRows: 2,
          widths: ['5%', '10%', '10%', '5%', '10%', '10%', '10%', '10%', '5%', '10%', '5%', '10%'],
          body: allRows
        }
      })
        const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'Portrait',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
          th: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3],
            bold: true
          },
          td: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          search: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          fertilizer: {
            margin: [0, 10, 0, 7]
          },
          fertilizerSHeader: {
              fontSize: 10,
              margin: [40, 0, 0, 0]
          },
          header: {
            fontSize: 12,
            margin: [0, 0, 0, 4]
          },
          header2: {
            fontSize: 14,
            margin: [0, 10, 0, 10]
          },
          headerPort1: {
            fontSize: 10,
            margin: [0, 20, 0, 5]
          },
          headerPort: {
            fontSize: 10,
            margin: [0, 4, 0, 15]
          },
          krishi: {
            margin: [0, -5, 0, 15],
            alignment: 'center'
          },
          header3: {
            fontSize: 9,
            margin: [0, 15, 0, 0]
          },
          address: {
            fontSize: 9,
            margin: [0, -10, 0, 0]
          },
          tableSubHead: {
            margin: [0, 5, 0, 15]
          }
        }
      }
      pdfMake.createPdf(docDefinition, null, null, null).download('task-report')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
