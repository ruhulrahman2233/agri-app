export default {
  userFind: (state) => (id) => state.users.find(user => user.id === id),
  pumpOperatorFind: (state) => (id) => state.pumpOperators.find(pumpOperator => pumpOperator.id === id),
  pumpInformationFind: (state) => (id) => state.pumpInformations.find(pumpInformation => pumpInformation.id === id),
  pumpSchedulerFind: (state) => (id) => state.pumpSchedulers.find(pumpScheduler => pumpScheduler.id === id)
}
