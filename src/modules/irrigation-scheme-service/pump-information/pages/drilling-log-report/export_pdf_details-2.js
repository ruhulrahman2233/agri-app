import i18n from '@/i18n'
import Store from '@/store'
// import { Time } from 'highcharts'
// import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'
// import { dateFormat } from '@/Utils/fliter'

const exportPdfDetails = async (reportTitle, data, thisObject) => {
  try {
    Store.commit('mutateCommonProperties', {
      loading: true
    })
    if (i18n.locale === 'bn') {
      pdfMake.vfs = pdfFontsBn.pdfMake.vfs
    } else {
      pdfMake.vfs = pdfFontsEn.pdfMake.vfs
    }
    // const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
    const pdfContent = [
      { text: reportTitle, style: 'header2', alignment: 'center' }
    ]

    const reportHeader = [
      [
        { text: '1', style: 'th', alignment: 'center', rowSpan: 5 },
        { text: '2', style: 'th', alignment: 'center', rowSpan: 5, colSpan: 2 },
        {},
        { text: '4', style: 'th', alignment: 'center' },
        { text: '5', style: 'th', alignment: 'center' },
        { text: '6', style: 'th', alignment: 'center' }
      ],
      [
        { text: '9', style: 'th', alignment: 'center' },
        { text: '10', style: 'th', alignment: 'center' },
        { text: '11', style: 'th', alignment: 'center' },
        { text: '12', style: 'th', alignment: 'center' },
        { text: '13', style: 'th', alignment: 'center' },
        { text: '14', style: 'th', alignment: 'center' }
      ],
      [
        { text: '17', style: 'th', alignment: 'center' },
        { text: '18', style: 'th', alignment: 'center' },
        { text: '19', style: 'th', alignment: 'center' },
        { text: '20', style: 'th', alignment: 'center' },
        { text: '21', style: 'th', alignment: 'center' },
        { text: '22', style: 'th', alignment: 'center' }
      ],
      [
        { text: '25', style: 'th', alignment: 'center' },
        { text: '26', style: 'th', alignment: 'center' },
        { text: '27', style: 'th', alignment: 'center' },
        { text: '28', style: 'th', alignment: 'center' },
        { text: '29', style: 'th', alignment: 'center' },
        { text: '30', style: 'th', alignment: 'center' }
      ],
      [
        {},
        {},
        {},
        { text: '36', style: 'th', alignment: 'center' },
        { text: '37', style: 'th', alignment: 'center' },
        { text: '38', style: 'th', alignment: 'center' }
      ],
      [
        { text: '41', style: 'th', alignment: 'center', rowSpan: 3, colSpan: 2 },
        { text: '42', style: 'th', alignment: 'center' },
        { text: '43', style: 'th', alignment: 'center', rowSpan: 3 },
        { text: '44', style: 'th', alignment: 'center' },
        { text: '45', style: 'th', alignment: 'center' },
        { text: '46', style: 'th', alignment: 'center' }
      ],
      [
        { text: '49', style: 'th', alignment: 'center' },
        { text: '50', style: 'th', alignment: 'center' },
        { text: '51', style: 'th', alignment: 'center' },
        { text: '52', style: 'th', alignment: 'center' },
        { text: '53', style: 'th', alignment: 'center' },
        { text: '54', style: 'th', alignment: 'center' }
      ],
      [
        {},
        {},
        {},
        { text: '60', style: 'th', alignment: 'center', colSpan: 3 },
        {},
        {}
      ]
    ]

    pdfContent.push({
      table: {
        headerRows: 1,
        widths: ['15%', '12%', '10%', '10%', '10%', '18%'],
        body: reportHeader
      }
    })

    const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
      var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'Portrait',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
          th: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3],
            bold: true
          },
          td: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          search: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          fertilizer: {
            margin: [0, 10, 0, 7]
          },
          fertilizerSHeader: {
              fontSize: 10,
              margin: [40, 0, 0, 0]
          },
          header: {
            fontSize: 12,
            margin: [0, 0, 0, 4]
          },
          header2: {
            fontSize: i18n.locale === 'bn' ? 14 : 12,
            margin: [0, 10, 0, 10]
          },
          header3: {
            fontSize: i18n.locale === 'bn' ? 13 : 11,
            margin: [0, 10, 0, 5]
          },
          headerPort1: {
            fontSize: 10,
            margin: [0, 20, 0, 5]
          },
          headerPort: {
            fontSize: 10,
            margin: [0, 4, 0, 15]
          },
          krishi: {
            margin: [0, -5, 0, 15],
            alignment: 'center'
          },
          address: {
            fontSize: 9,
            margin: [0, -10, 0, 0]
          },
          tableSubHead: {
            margin: [0, 5, 0, 15]
          }
        }
      }
      pdfMake.createPdf(docDefinition, null, null, null).download('drilling-log-details-report')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
