// Pusti Parameter
export const mappingParameterList = '/pusti-mapping/mapping-parameter/list'
export const mappingParameterStore = '/pusti-mapping/mapping-parameter/store'
export const mappingParameterUpdate = '/pusti-mapping/mapping-parameter/update'
export const mappingParameterStatus = '/pusti-mapping/mapping-parameter/toggle-status'
export const mappingParameterDestroy = '/pusti-mapping/mapping-parameter/destroy'
export const mappingParameterData = '/pusti-mapping/mapping-parameter/get_data'
// Mapping Parameter Pdf
export const MappingParameterPdf = '/pusti-mapping/mapping-parameter-pdf'
// Mapping Parameter Report
export const MappingParameterReport = '/pusti-mapping/mapping-parameter-report'
// Mapping Form
export const MappingFormList = '/pusti-mapping/mapping-form/list'
export const mappingFormStatus = '/pusti-mapping/mapping-form/toggle-status'
export const mappingFormDestroy = '/pusti-mapping/mapping-form/destroy'
export const MappingFormSocial = '/pusti-mapping/mapping-form/social_info'
export const MappingFormPhysical = '/pusti-mapping/mapping-form/physical_info'
export const MappingFormHealth = '/pusti-mapping/mapping-form/health_info'
// MappingForm Data Gather
export const mappingFormData = '/pusti-mapping/mapping-form/get_data'
export const MappingFormStatus = '/pusti-mapping/mapping-form/toggle-status'
