import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'

function getGender (data, vm) {
  const gdata = []
  for (const key of Object.entries(data)) {
    const k = key
    if (k[0] === '1') {
      gdata.push({ text: vm.$t('pusti_mapping.boy'), style: 'tdCenter', alignment: 'left' })
    } else {
      gdata.push({ text: vm.$t('pusti_mapping.girl'), style: 'tdCenter', alignment: 'left' })
    }
  }
  return gdata
}

function getWRange (data, vm) {
  const gdata = []
  for (const key of Object.entries(data)) {
    const k = key
    Object.values(k[1]).forEach(val => {
      gdata.push({ text: val.weight_range_from + ' - ' + val.weight_range_to, style: 'tdleft', alignment: 'left' })
    })
  }
  return gdata
}

function getHRange (data, vm) {
  const gdata = []
  for (const key of Object.entries(data)) {
    const k = key
    Object.values(k[1]).forEach(val => {
      gdata.push({ text: val.height_range_from + ' - ' + val.height_range_to, style: 'tdleft', alignment: 'left' })
    })
  }
  return gdata
}

function getZScore (data, vm) {
  const gdata = []
  for (const key of Object.entries(data)) {
    const k = key
    Object.values(k[1]).forEach(val => {
      gdata.push({ text: val.z_score, style: 'tdleft', alignment: 'left' })
    })
  }
  return gdata
}

const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, reportdata, vm) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
      const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
      const pdfContent = [
        {
            columns: reportHeadData.reportHeadColumn, style: 'main_head'
        },
        {
          text: vm.$t('fertilizerReport.krishi_bhaban'),
          style: 'krishi',
          alignment: 'center'
        },
        { text: reportHeadData.address, style: 'address', alignment: 'center' }
      ]

      if (reportHeadData.projectName) {
        pdfContent.push({ text: reportHeadData.projectName, style: 'header3', alignment: 'center' })
      }
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        var allrow1 = []
        var tabledata = [
          { text: vm.$t('globalTrans.sl_no'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.month'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.gender'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.weight_range'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.z_score'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.height_range'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.z_score'), style: 'th', alignment: 'center' }
        ]
        allrow1.push(tabledata)
        reportdata.forEach((report, index) => {
          for (const [key, value] of Object.entries(report)) {
            tabledata = [
              { text: index + 1, style: 'tdleft', alignment: 'left' },
              { text: key, style: 'tdleft', alignment: 'center' },
              [getGender(value, vm)],
              [getWRange(value, vm)],
              [getZScore(value, vm)],
              [getHRange(value, vm)],
              [getZScore(value, vm)]
            ]
            allrow1.push(tabledata)
          }
        })
        pdfContent.push(
          {
            table: {
                headerRows: 0,
                margin: 'auto',
                style: 'main_head',
                widths: ['5%', '*', '*', '*', '*', '*', '*'],
                body: allrow1,
                alignment: 'center'
            }
          }
        )
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'Landscape',
        styles: {
            subheader: {
              bold: true,
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            th: {
              fillColor: '#dee2e6',
              bold: true,
              fontSize: 10,
              margin: [6, 6, 6, 6]
            },
            tdleft: {
              fontSize: 8,
              margin: [1, 1, 1, 1],
              bold: true,
              border: 0,
              alignment: 'center'
            },
            tdCenter: {
              fontSize: 8,
              margin: [6, 6, 6, 6],
              bold: true,
              border: 0
            },
            td: {
              fontSize: 8,
              margin: [1, 1, 1, 1],
              border: 0
            },
            header: {
              fontSize: 12,
              margin: [0, 0, 0, 4]
            },
            header2: {
              border: 0,
              fontSize: 10,
              margin: [0, 10, 0, 20]
            },
            header3: {
            fontSize: 9,
            margin: [0, 0, 0, 4]
            },
            tableSubHead: {
              margin: [0, 5, 0, 15]
            }
          }
        }
        pdfMake.createPdf(docDefinition, null, null, null).download()
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
