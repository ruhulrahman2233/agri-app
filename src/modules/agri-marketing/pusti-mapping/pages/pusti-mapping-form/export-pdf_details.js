import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'

const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, infodata, vm, sdata, pdata, hdata) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
      const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
      const pdfContent = [
        {
            columns: reportHeadData.reportHeadColumn, style: 'main_head'
        },
        {
          text: vm.$t('fertilizerReport.krishi_bhaban'),
          style: 'krishi',
          alignment: 'center'
        },
        { text: reportHeadData.address, style: 'address', alignment: 'center' }
      ]

      if (reportHeadData.projectName) {
        pdfContent.push({ text: reportHeadData.projectName, style: 'header3', alignment: 'center' })
      }
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        pdfContent.push(
          {
            table: {
              style: 'tableData',
              headerRows: 0,
              widths: ['10%', '10%', '60%', '10%', '10%'],
              body: [
                [
                  { text: vm.$t('pusti_mapping.pusti_mapping_id'), style: 'subheader' },
                  { text: infodata.form_id, style: 'subheader' },
                  { text: vm.$t('pusti_mapping.mapping_form'), style: 'subheader', alignment: 'center' },
                  { text: vm.$t('pusti_mapping.pusti_mapping_date'), style: 'subheader' },
                  { text: infodata.form_date, style: 'subheader' }
                ]
              ]
            },
            layout: {
              hLineWidth: function (i, node) {
                return 0
              },
              vLineWidth: function (i, node) {
                return 0
              }
            }
          }
        )
        var allRow1 = []
        var tabledata = []
        tabledata = [
          { text: vm.$t('pusti_mapping.socio_economic_info'), style: 'th', colSpan: '2', alignment: 'left' },
          {}
        ]
        allRow1.push(tabledata)
        Object.keys(sdata).forEach(key => {
          tabledata = [
            { text: vm.$t('pusti_mapping.' + key), style: 'tdleft', alignment: 'left' },
            { text: sdata[key], style: 'td', alignment: 'left' }
          ]
          allRow1.push(tabledata)
        })
        pdfContent.push(
          {
            table: {
                headerRows: 1,
                margin: 0,
                border: 0,
                style: 'header2',
                widths: ['30%', '70%'],
                body: allRow1
            }
          }
        )
        var allRow2 = []
        var tabledata2 = []
        tabledata2 = [
          { text: vm.$t('pusti_mapping.physical_measurment'), style: 'th', colSpan: '2', alignment: 'left' },
          {}
        ]
        allRow2.push(tabledata2)
        Object.keys(pdata).forEach(key => {
          tabledata2 = [
            { text: vm.$t('pusti_mapping.' + key), style: 'tdleft', alignment: 'left' },
            { text: pdata[key], style: 'td', alignment: 'left' }
          ]
          allRow2.push(tabledata2)
        })
        pdfContent.push(
          {
            table: {
                headerRows: 1,
                margin: 0,
                border: 0,
                style: 'header2',
                widths: ['30%', '70%'],
                body: allRow2
            }
          }
        )
        var allRow3 = []
        var tabledata3 = []
        tabledata3 = [
          { text: vm.$t('pusti_mapping.health_measurment'), style: 'th', colSpan: '2', alignment: 'left' },
          {}
        ]
        allRow3.push(tabledata3)
        Object.keys(hdata).forEach(key => {
          tabledata3 = [
            { text: vm.$t('pusti_mapping.' + key), style: 'tdleft', alignment: 'left' },
            { text: hdata[key], style: 'td', alignment: 'left' }
          ]
          allRow3.push(tabledata3)
        })
        pdfContent.push(
          {
            table: {
                headerRows: 1,
                margin: 0,
                border: 0,
                style: 'header2',
                widths: ['30%', '70%'],
                body: allRow3
            }
          }
        )
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'Landscape',
        styles: {
            subheader: {
              bold: true,
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            th: {
              fillColor: '#dee2e6',
              bold: true,
              fontSize: 10,
              margin: [6, 6, 6, 6]
            },
            tdleft: {
              fontSize: 8,
              margin: [1, 1, 1, 1],
              bold: true,
              border: 0
            },
            td: {
              fontSize: 8,
              margin: [1, 1, 1, 1],
              border: 0
            },
            header: {
              fontSize: 12,
              margin: [0, 0, 0, 4]
            },
            header2: {
              border: 0,
              fontSize: 10,
              margin: [0, 10, 0, 20]
            },
            header3: {
            fontSize: 9,
            margin: [0, 0, 0, 4]
            },
            tableSubHead: {
              margin: [0, 5, 0, 15]
            },
            krishi: {
              margin: [0, 1, 0, 15],
              alignment: 'center'
            },
            address: {
              fontSize: 9,
              margin: [0, -10, 0, 0]
            },
            report_type: {
              fontSize: 9,
              margin: [0, 2, 0, 15]
            }
          }
        }
        pdfMake.createPdf(docDefinition, null, null, null).download()
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
