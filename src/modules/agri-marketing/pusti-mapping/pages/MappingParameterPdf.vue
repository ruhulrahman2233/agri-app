<template>
    <b-container fluid>
        <b-row>
            <b-col md="12">
        <iq-card>
            <template v-slot:headerTitle>
                <h4 class="card-title">{{ $t('pusti_mapping.mapping_parameter') }}</h4>
            </template>
            <template v-slot:body>
                <b-row>
                    <b-col xs="12" sm="12" md="6" lg="6" xl="6">
                        <b-form-group
                            class="row"
                            label-cols-sm="3"
                            :label="$t('pusti_mapping.gender')"
                            label-for="gender"
                            >
                            <b-form-select
                                plain
                                v-model="search.gender"
                                :options="genderList"
                                id="gender"
                                >
                                <template v-slot:first>
                                <b-form-select-option :value="0" >{{$t('globalTrans.select')}}</b-form-select-option>
                                </template>
                            </b-form-select>
                        </b-form-group>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col xs="12" sm="12" md="6" lg="6" xl="6">
                        <b-form-group
                            class="row"
                            label-cols-sm="3"
                            :label="$t('pusti_mapping.month_from')"
                            label-for="month_from"
                            >
                            <b-form-input id="month_from" v-model="search.month_from"></b-form-input>
                        </b-form-group>
                    </b-col>
                </b-row>
                <b-row>
                    <b-col xs="12" sm="12" md="6" lg="6" xl="6">
                        <b-form-group
                            class="row"
                            label-cols-sm="3"
                            :label="$t('pusti_mapping.month_to')"
                            label-for="month_to"
                            >
                            <b-form-input id="month_to" v-model="search.month_to"></b-form-input>
                        </b-form-group>
                    </b-col>
                </b-row>
                <b-button type="button" variant="primary" @click="searchData">{{ $t('globalTrans.search') }}</b-button>
            </template>
        </iq-card>
                <iq-card>
                    <template v-slot:headerTitle>
                        <h4 class="card-title">{{ $t('pusti_mapping.mapping_parameter_list') }}</h4>
                    </template>
                    <template v-slot:headerAction>
                      <b-button variant="primary" @click="pdfExport(data)" class="mr-2">
                        {{  $t('globalTrans.export_pdf') }}
                      </b-button>
                    </template>
                    <template v-slot:body>
                        <b-overlay :show="loadingState">
                            <b-row>
                                <b-col md="12" class="table-responsive">
                                    <b-table :emptyText="$t('globalTrans.noDataFound')" show-empty bordered hover :items="listData" :fields="columns" aria-hidden="loading | listReload ? 'true' : null">
                                        <template v-slot:cell(index)="data">
                                            {{ data.index + 1}}
                                        </template>
                                        <template v-slot:cell(month)="data">
                                            <span v-for="(item, idx) in data.item" :key="idx">{{ idx }}</span>
                                        </template>
                                        <template v-slot:cell(gender)="data">
                                            <div v-for="(item, idx) in data.item" :key="idx">
                                              <b-tr v-for="(item, idx) in item" :key="idx">
                                                <b-td class="b-0">
                                                  <span v-if="idx == 1">{{$t('pusti_mapping.boy')}}</span>
                                                  <span  v-else>{{$t('pusti_mapping.girl')}}</span>
                                                </b-td>
                                              </b-tr>
                                            </div>
                                        </template>
                                        <template v-slot:cell(weight_range)="data">
                                            <div v-for="(item, idx) in data.item" :key="idx">
                                              <div v-for="(item, idx) in data.item" :key="idx">
                                                <div v-for="(item, idx) in item" :key="idx" class="mb-2">
                                                  <div v-for="(item, idx) in item" :key="idx">
                                                      <span>{{ item.weight_range_from }}</span> - <span>{{ item.weight_range_to }}</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </template>
                                        <template v-slot:cell(height_range)="data">
                                            <div v-for="(item, idx) in data.item" :key="idx">
                                              <div v-for="(item, idx) in data.item" :key="idx">
                                                <div v-for="(item, idx) in item" :key="idx" class="mb-2">
                                                  <div v-for="(item, idx) in item" :key="idx">
                                                    <span>{{ item.height_range_from }}</span> - <span>{{ item.height_range_to }}</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </template>
                                        <template v-slot:cell(z_score)="data">
                                            <div v-for="(item, idx) in data.item" :key="idx">
                                              <div v-for="(item, idx) in data.item" :key="idx">
                                                <div v-for="(item, idx) in item" :key="idx">
                                                  <div v-for="(item, idx) in item" :key="idx">
                                                    <span>{{ item.z_score }}</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                        </template>
                                        <template v-slot:cell(action)="data">
                                            <span>
                                                <b-button variant=" iq-bg-success" size="sm" @click="pdfExport(data.item)"><i class="fas fa-download m-0"></i></b-button>
                                            </span>
                                        </template>
                                    </b-table>
                                    <b-pagination
                                        v-model="pagination.currentPage"
                                        :per-page="pagination.perPage"
                                        :total-rows="pagination.totalRows"
                                        @input="searchData"
                                        />
                                </b-col>
                            </b-row>
                        </b-overlay>
                    </template>
                </iq-card>
            </b-col>
        </b-row>
    </b-container>
</template>
<script>
import RestApi, { agriMarketingServiceBaseUrl } from '@/config/api_config'
import { MappingParameterPdf } from '../api/routes'
import ModalBaseMasterList from '@/mixins/modal-base-master-list'
import StaticData from './mapping-parameter/static_data.js'
import ExportPdf from './export-pdf_details'
export default {
  mixins: [ModalBaseMasterList],
  data () {
    return {
      search: {
        gender: '',
        month_from: '',
        month_to: ''
      },
      rows: [],
      data: ''
    }
  },
  computed: {
    genderList: function () {
      return StaticData.genderList(this)
    },
    columns () {
      const labels = [
          { label: this.$t('globalTrans.sl_no'), class: 'text-left' },
          { label: this.$t('pusti_mapping.month'), class: 'text-left' },
          { label: this.$t('pusti_mapping.gender'), class: 'text-left' },
          { label: this.$t('pusti_mapping.weight_range'), class: 'text-left' },
          { label: this.$t('pusti_mapping.height_range'), class: 'text-left' },
          { label: this.$t('pusti_mapping.z_score'), class: 'text-left' }
        ]

      let keys = []

      if (this.$i18n.locale === 'bn') {
        keys = [
          { key: 'index' },
          { key: 'month' },
          { key: 'gender' },
          { key: 'weight_range' },
          { key: 'height_range' },
          { key: 'z_score' }
        ]
      } else {
        keys = [
          { key: 'index' },
          { key: 'month' },
          { key: 'gender' },
          { key: 'weight_range' },
          { key: 'height_range' },
          { key: 'z_score' }
        ]
      }

      return labels.map((item, index) => {
          return Object.assign(item, keys[index])
        })
    }
  },
  watch: {
    loadingState: function (newVal, oldVal) {
      if (newVal) {
        this.loadData()
      }
    }
  },
  created () {
    this.loadData()
  },
  methods: {
    searchData () {
      this.loadData()
    },
    pdfExport (item) {
      const printData = item
      const reportTitle = this.$t('pusti_mapping.mapping_parameter_list')
      ExportPdf.exportPdfDetails(agriMarketingServiceBaseUrl, '/cotton/config/report-heading/detail', 3, reportTitle, printData, this)
    },
    loadData () {
      const params = Object.assign({}, this.search, { page: this.pagination.currentPage, per_page: this.pagination.perPage })
      this.$store.dispatch('mutateCommonProperties', { loading: true, listReload: false })
      RestApi.getData(agriMarketingServiceBaseUrl, MappingParameterPdf, params).then(response => {
        if (response.success) {
            this.$store.dispatch('setList', response.data)
            this.paginationData(response.data)
            this.data = response.data
        }
      })
      this.$store.dispatch('mutateCommonProperties', { loading: false, listReload: false })
    }
  }
}
</script>
