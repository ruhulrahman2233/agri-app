const genderList = (vm) => {
    return [
        {
          value: '1',
          text_en: 'Boy',
          text_bn: 'ছেলে',
          text: vm.$t('pusti_mapping.boy'),
          status: 0
        },
        {
          value: '2',
          text_en: 'Girl',
          text_bn: 'মেয়ে',
          text: vm.$t('pusti_mapping.girl'),
          status: 0
        }
    ]
}
const divisionGraph = (vm) => {
    return [
        {
          text: 'Dhaka',
          text_graph: 'bd-da'
        },
        {
          text: 'Rajshahi',
          text_graph: 'bd-rj'
        },
        {
          text: 'Khulna',
          text_graph: 'bd-kh'
        },
        {
          text: 'Barisal',
          text_graph: 'bd-ba'
        },
        {
          text: 'Chattagram',
          text_graph: 'bd-cg'
        },
        {
          text: 'Rangpur',
          text_graph: 'bd-rp'
        },
        {
          text: 'Sylhet',
          text_graph: 'bd-sy'
        },
        {
          text: 'Mymensingh',
          text_graph: 'bd-my'
        }
    ]
}
export default {
  genderList,
  divisionGraph
}
