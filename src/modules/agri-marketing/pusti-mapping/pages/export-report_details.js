import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'

function getDivisionName (divisionId = null) {
  const gdata = []
  const divid = parseInt(divisionId)
  const divisfind = Store.state.commonObj.divisionList.filter(division => division.value === divid)
  gdata.push({ text: divisfind[0].text, style: 'tdleft', alignment: 'left' })
  return gdata
}
function getDistrictName (data) {
  const gdata = []
  Object.keys(data).forEach(key => {
    const disid = parseInt(key)
    const divisfind = Store.state.commonObj.districtList.filter(division => division.value === disid)
    const disName = divisfind[0].text
    gdata.push({ text: disName, style: 'tdleft', alignment: 'left' })
  })
  return gdata
}
function getTCount (data) {
  const gdata = []
  Object.values(data).forEach(value => {
    gdata.push({ text: value.length, style: 'tdleft', alignment: 'left' })
  })
  return gdata
}
function getMCount (data) {
  let mdata = 0
  Object.values(data).forEach(value => {
    let count = 0
    value.map((item, index) => {
      if (item.gender === 1) {
        ++count
      }
    })
    mdata = count
  })
  return { text: mdata, style: 'tdleft', alignment: 'left' }
}
function getFCount (data) {
  let mdata = 0
  Object.values(data).forEach(value => {
    let count = 0
    value.map((item, index) => {
      if (item.gender === 2) {
        ++count
      }
    })
    mdata = count
  })
  return { text: mdata, style: 'tdleft', alignment: 'left' }
}
function getSAMCount (data) {
  let mdata = 0
  Object.values(data).forEach(value => {
    let count = 0
    value.map((item, index) => {
      if (item.physical_info.muac === 1) {
        ++count
      }
    })
    mdata = count
  })
  return { text: mdata, style: 'tdleft', alignment: 'left' }
}
function getMAMCount (data) {
  let mdata = 0
  Object.values(data).forEach(value => {
    let count = 0
    value.map((item, index) => {
      if (item.physical_info.muac === 2) {
        ++count
      }
    })
    mdata = count
  })
  return { text: mdata, style: 'tdleft', alignment: 'left' }
}
function getUCount (data) {
  let mdata = 0
  Object.values(data).forEach(value => {
    let count = 0
    value.map((item, index) => {
      if (item.health_info.question_no_5 === 1) {
        ++count
      }
    })
    mdata = count
  })
  return { text: mdata, style: 'tdleft', alignment: 'left' }
}
const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, reportdata, vm) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
      const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
      const pdfContent = [
        {
            columns: reportHeadData.reportHeadColumn, style: 'main_head'
        },
        {
          text: vm.$t('fertilizerReport.krishi_bhaban'),
          style: 'krishi',
          alignment: 'center'
        },
        { text: reportHeadData.address, style: 'address', alignment: 'center' }
      ]

      if (reportHeadData.projectName) {
        pdfContent.push({ text: reportHeadData.projectName, style: 'header3', alignment: 'center' })
      }
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        var allrow1 = []
        var tabledata = [
          { text: vm.$t('globalTrans.sl_no'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.division_id'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.district_id'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.total_population'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.male'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.female'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.sam_r'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.mam_r'), style: 'th', alignment: 'center' },
          { text: vm.$t('pusti_mapping.underweight'), style: 'th', alignment: 'center' }
        ]
        allrow1.push(tabledata)
        reportdata.forEach((report, index) => {
          for (const [key, value] of Object.entries(report)) {
            tabledata = [
              { text: index + 1, style: 'tdleft', alignment: 'left' },
              getDivisionName(key),
              getDistrictName(value),
              getTCount(value),
              getMCount(value),
              getFCount(value),
              getSAMCount(value),
              getMAMCount(value),
              getUCount(value)
            ]
            allrow1.push(tabledata)
          }
        })
        pdfContent.push(
          {
            table: {
                headerRows: 0,
                margin: 'auto',
                style: 'header2',
                widths: ['5%', '*', '*', '*', '*', '*', '*', '*', '*'],
                body: allrow1
            }
          }
        )
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'Landscape',
        styles: {
            subheader: {
              bold: true,
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            th: {
              fillColor: '#dee2e6',
              bold: true,
              fontSize: 10,
              margin: [6, 6, 6, 6]
            },
            tdleft: {
              fontSize: 8,
              margin: [1, 1, 1, 1],
              bold: true,
              border: 0,
              alignment: 'center'
            },
            tdCenter: {
              fontSize: 8,
              margin: [6, 6, 6, 6],
              bold: true,
              border: 0
            },
            td: {
              fontSize: 8,
              margin: [1, 1, 1, 1],
              border: 0
            },
            header: {
              fontSize: 12,
              margin: [0, 0, 0, 4]
            },
            header2: {
              border: 0,
              fontSize: 10,
              margin: [0, 10, 0, 20]
            },
            header3: {
            fontSize: 9,
            margin: [0, 0, 0, 4]
            },
            tableSubHead: {
              margin: [0, 5, 0, 15]
            }
          }
        }
        pdfMake.createPdf(docDefinition, null, null, null).download()
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
