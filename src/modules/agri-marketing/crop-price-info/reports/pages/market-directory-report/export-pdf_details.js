import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'
// import { dateFormat } from '@/Utils/fliter'
function getHattDay (datDay) {
  datDay = parseInt(datDay)
  if (datDay === 1) {
      return i18n.locale === 'en' ? 'Daily' : 'প্রতিদিন'
  } else if (datDay === 2) {
        return i18n.locale === 'en' ? 'Friday' : 'শুক্রবার'
  } else if (datDay === 3) {
      return i18n.locale === 'en' ? 'Saturday' : 'শনিবার'
  } else if (datDay === 4) {
      return i18n.locale === 'en' ? 'Sunday' : 'রবিবার'
  } else if (datDay === 5) {
    return i18n.locale === 'en' ? 'Monday' : 'সোমবার'
  } else if (datDay === 6) {
    return i18n.locale === 'en' ? 'Tuesday' : 'মঙ্গলবার'
  } else if (datDay === 7) {
    return i18n.locale === 'en' ? 'Wednesday' : 'বুধবার'
  } else if (datDay === 8) {
    return i18n.locale === 'en' ? 'Thursday' : 'বৃহস্পতিবার'
  }
}
const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, newDatas, vm, search) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
      const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
      const pdfContent = [
          {
              columns: reportHeadData.reportHeadColumn, style: 'main_head'
          },
          { text: reportHeadData.address, style: 'address', alignment: 'center' }
        ]
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        pdfContent.push({ text: '', style: 'fertilizer' })
        const allRows = [
          [
            { text: vm.$t('globalTrans.division'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('globalTrans.district'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('globalTrans.upazila'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.market'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.foundation_year'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.Village'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.PostOffice'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.p_t_u'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.MarketType'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.GovtCovered'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.GovtOpen'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.PrivateCovered'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.PrivateOpen'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.ShedNo'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.ShedArea'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.StallCountAgr'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.StallCountNonAgr'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.HatDays'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.market_time_from'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.market_time_to'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.BuyerSellecount'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.ProduceAssemblage'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.FarmerShare'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.TraderShare'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.average_distrance'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.data_collection_year'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.name'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.tel_mobile_number'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.Latitude'), style: 'th', alignment: 'center', bold: true },
            { text: vm.$t('crop_price_config.Longitude'), style: 'th', alignment: 'center', bold: true }
          ]
        ]
        newDatas.forEach((info, index) => {
          let hatDays = []
          info.hat_days.forEach((info1, index1) => {
            if (index1 === 0) {
              hatDays = getHattDay(info1)
            } else {
              hatDays = hatDays + ',' + getHattDay(info1)
            }
          })
          allRows.push([
            { text: (i18n.locale === 'bn') ? info.division_name_bn : info.division_name, alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.distict_name_bn : info.distict_name, alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.upazila_name_bn : info.upazila_name, alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.market_name_bn : info.market_name, alignment: 'center', style: 'td' },
            { text: vm.$n(info.foundation_year), alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.village : info.village, alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.post_office : info.post_office, alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.union : info.union, alignment: 'center', style: 'td' },
            { text: vm.getMarket(info.market_type), alignment: 'center', style: 'td' },
            { text: vm.$n(info.govt_covered), alignment: 'center', style: 'td' },
            { text: vm.$n(info.govt_open), alignment: 'center', style: 'td' },
            { text: vm.$n(info.private_covered), alignment: 'center', style: 'td' },
            { text: vm.$n(info.private_open), alignment: 'center', style: 'td' },
            { text: vm.$n(info.shed_no), alignment: 'center', style: 'td' },
            { text: vm.$n(info.shed_area), alignment: 'center', style: 'td' },
            { text: vm.$n(info.stall_no_agri), alignment: 'center', style: 'td' },
            { text: vm.$n(info.stall_no_nonagri), alignment: 'center', style: 'td' },
            { text: hatDays, alignment: 'center', style: 'td' },
            { text: vm.$t(info.market_time_from), alignment: 'center', style: 'td' },
            { text: vm.$t(info.market_time_to), alignment: 'center', style: 'td' },
            { text: vm.$n(info.number_of_buyers), alignment: 'center', style: 'td' },
            { text: vm.$n(info.number_of_sellers), alignment: 'center', style: 'td' },
            { text: vm.$n(info.farmer_share), alignment: 'center', style: 'td' },
            { text: vm.$n(info.trader_share), alignment: 'center', style: 'td' },
            { text: vm.$n(info.avg_distance), alignment: 'center', style: 'td' },
            { text: vm.$n(info.data_collection_year), alignment: 'center', style: 'td' },
            { text: (i18n.locale === 'bn') ? info.market_representative_name : info.market_representative_name, alignment: 'center', style: 'td' },
            { text: vm.$n(info.market_representative_mobile), alignment: 'center', style: 'td' },
            { text: vm.$n(info.latitude), alignment: 'center', style: 'td' },
            { text: vm.$n(info.longitude), alignment: 'center', style: 'td' }
          ])
        })
        pdfContent.push({
          table: {
            headerRows: 1,
            widths: ['4%', '4%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '5%', '5%', '5%', '5%', '3%', '3%', '4%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%', '3%'],
            body: allRows
          }
        })
        const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A2',
        pageOrientation: 'landscape',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
          th: {
            fontSize: (i18n.locale === 'bn') ? 10 : 9,
            margin: [3, 3, 3, 3]
          },
          td: {
            fontSize: (i18n.locale === 'bn') ? 10 : 9,
            margin: [3, 3, 3, 3]
          },
          search: {
            fontSize: (i18n.locale === 'bn') ? 10 : 8,
            margin: [3, 3, 3, 3]
          },
          fertilizer: {
            margin: [5, 0, 0, 5]
          },
          header: {
            fontSize: 12,
            margin: [0, 0, 0, 4]
          },
          header2: {
            fontSize: 14,
            margin: [0, 10, 0, 20]
          },
          headerPort1: {
            fontSize: 10,
            margin: [0, 20, 0, 5]
          },
          headerPort: {
            fontSize: 10,
            margin: [0, 4, 0, 15]
          },
          krishi: {
            margin: [0, -5, 0, 15],
            alignment: 'center'
          },
          header3: {
            fontSize: 9,
            margin: [0, 0, 0, 0]
          },
          address: {
            fontSize: 9,
            margin: [0, -10, 0, 0]
          },
          tableSubHead: {
            margin: [0, 5, 0, 15]
          }
        }
      }
      pdfMake.createPdf(docDefinition, null, null, null).download('market-directory-report')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
