// Price Collection Verify routes
const priceInfo = 'crop-price-info/config/cpi-price-infos/'
export const priceCollVerifyList = priceInfo + 'price-collection-verify-list'
export const priceCollVerifyDetailList = priceInfo + 'list'
export const priceCollVerify = priceInfo + 'price-collection-verify'
export const priceCollPriceUpdate = priceInfo + 'price-update'
export const priceCollPriceView = priceInfo + 'price-view'
export const getFiscalYear = 'master-fiscal-year/get-fiscal-year-by-date'

// Price Entry
export const PriceEntryList = '/crop-price-info/cpi/price-entry/list'
export const MarketPriceList = '/crop-price-info/cpi/price-entry/market-price-list'
export const marketGrowerPriceList = '/crop-price-info/cpi/price-entry/market-grower-price-list'
export const marketWeeklyPriceList = '/crop-price-info/cpi/price-entry/market-weekly-price-list'
export const PriceEntryStore = '/crop-price-info/cpi/price-entry/store'
export const PriceEntryUpdate = '/crop-price-info/cpi/price-entry/update'
