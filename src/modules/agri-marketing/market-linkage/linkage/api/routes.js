// market linkage routes
const marketLinkage = '/market-linkage/linkage'
export const groBuyProList = marketLinkage + '/grower-buyer-profile/list'
export const groBuyProStore = marketLinkage + '/grower-buyer-profile/store'
export const groBuyProUpdate = marketLinkage + '/grower-buyer-profile/update'
export const groBuyProApprove = marketLinkage + '/grower-buyer-profile/approve'
export const groBuyProReject = marketLinkage + '/grower-buyer-profile/reject'
export const groBuyProDestroy = marketLinkage + '/grower-buyer-profile/destroy'
export const productInfoStore = marketLinkage + '/product-info/store'
// market linkage dashboard route
export const marketLinkageDashboard = '/market-linkage/dashboard/list'
