// Commodity Type
export const commodityTypeList = '/market-linkage/config/commodity-type/list'
export const commodityTypeStore = '/market-linkage/config/commodity-type/store'
export const commodityTypeUpdate = '/market-linkage/config/commodity-type/update'
export const commodityTypeStatus = '/market-linkage/config/commodity-type/toggle-status'
export const commodityTypeDestroy = '/market-linkage/config/commodity-type/destroy'
