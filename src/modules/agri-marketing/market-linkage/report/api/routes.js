// Mapping Parameter Pdf
// export const MappingParameterPdf = '/pusti-mapping/mapping-parameter-pdf'

// farmers information list
export const FarmerInformationList = '/market-linkage/reports/farmers-info'
export const ProductInformationList = '/market-linkage/reports/product-info'
export const BuyersInformationList = '/market-linkage/reports/buyers-info'
export const marketInformationList = '/market-linkage/reports/market-info'
export const reportHeadingList = '/cotton/config/report-heading/detail'
