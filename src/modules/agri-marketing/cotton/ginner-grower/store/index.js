import getters from './getters'
import actions from './actions'
import mutations from './mutations'

/**
 * This store will be used for all modules of this component
 */
const state = {
    growerDropdownList: {}
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
