// Cotton Production Reoprt
export const CottonProductionReportList = '/cotton/reports/cotton-production/list'
export const FiscalYearWiseCottonSellReportList = '/cotton/reports/fiscal-year-wise-cotton-sell/list'
export const GrowerInformationReportList = '/cotton/reports/grower-information/list'
export const reportHeadingList = '/cotton/config/report-heading/detail'
export const ginnerInformationReportList = '/cotton/reports/ginner-information/list'
export const CottonStockReport = '/cotton/reports/cotton-stock-report/list'
export const CottonProductionDashboard = '/cotton/reports/cotton-production/dashboard'
