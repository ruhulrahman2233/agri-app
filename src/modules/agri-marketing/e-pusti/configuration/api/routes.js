// divisional Office
export const divisionalOfficeList = '/e-pusti/config/divisional-office/list'
export const userStore = '/user/store'
export const divisionalOfficeStore = '/e-pusti/config/divisional-office/store'
export const divisionalOfficeUpdate = '/e-pusti/config/divisional-office/update'
export const divisionalOfficeStatus = '/e-pusti/config/divisional-office/toggle-status'
export const divisionalOfficeDestroy = '/e-pusti/config/divisional-office/destroy'

// Campagin Name
export const campaignList = '/e-pusti/config/master-campaign/list'
export const campaignStore = '/e-pusti/config/master-campaign/store'
export const campaignUpdate = '/e-pusti/config/master-campaign/update'
export const campaignStatus = '/e-pusti/config/master-campaign/toggle-status'
export const campaignDestroy = '/e-pusti/config/master-campaign/destroy'
export const campaignDashboard = '/e-pusti/campaign-dashboard'
