// campaign schedule assign
const campaignScheduleBasePath = '/e-pusti/birtan/campaign-schedule/'
export const campaignScheduleList = campaignScheduleBasePath + 'list'
export const campaignScheduleStore = campaignScheduleBasePath + 'store'
export const campaignScheduleUpdate = campaignScheduleBasePath + 'update'
export const campaignScheduleToggle = campaignScheduleBasePath + 'toggle-status'
export const campaignScheduleDestroy = campaignScheduleBasePath + 'destroy'
    // ha Manage
export const unitList = '/cotton/config/units/list'
export const unitStore = '/cotton/config/units/store'
export const unitUpdate = '/cotton/config/units/update'
export const unitStatus = '/cotton/config/units/toggle-status'
export const unitDestroy = '/cotton/config/units/destroy'

// Manage Campaign Event
export const ManageCampaignEventList = '/e-pusti/campaign-events/list'
export const ManageCampaignEventStore = '/e-pusti/campaign-events/store'
export const ManageCampaignEventUpdate = '/e-pusti/campaign-events/update'
export const ManageCampaignEventStatus = '/e-pusti/campaign-events/toggle-status'
export const ManageCampaignEventDestroy = '/e-pusti/campaign-events/destroy'

// campaignRequest
export const campaignRequestList = '/e-pusti/campaign-requests/list'
export const campaignRequestStore = '/e-pusti/campaign-requests/store'
export const campaignRequestUpdate = '/e-pusti/campaign-requests/update'
export const campaignRequestStatus = '/e-pusti/campaign-requests/toggle-status'
export const campaignRequestDestroy = '/e-pusti/campaign-requests/destroy'

// Campaign Calendar
export const CampaignCalendarList = '/e-pusti/campaign-calendars/list'
export const CampaignCalendarStore = '/e-pusti/campaign-calendars/store'
export const CampaignCalendarUpdate = '/e-pusti/campaign-calendars/update'
export const CampaignCalendarStatus = '/e-pusti/campaign-calendars/toggle-status'
export const CampaignCalendarDestroy = '/e-pusti/campaign-calendars/destroy'

// Campaign Attendance
export const CampaignAttendanceList = '/e-pusti/campaign-attendances/list'
export const CampaignAttendanceStore = '/e-pusti/campaign-attendances/store'
export const CampaignAttendanceUpdate = '/e-pusti/campaign-attendances/update'
export const CampaignAttendanceStatus = '/e-pusti/campaign-attendances/toggle-status'
export const CampaignAttendanceDestroy = '/e-pusti/campaign-attendances/destroy'

// Campaign Feedback
export const CampaignFeedbackList = '/e-pusti/campaign-feedbacks/list'
export const CampaignFeedbackStore = '/e-pusti/campaign-feedbacks/store'
export const CampaignFeedbackUpdate = '/e-pusti/campaign-feedbacks/update'
export const CampaignFeedbackStatus = '/e-pusti/campaign-feedbacks/toggle-status'
export const CampaignFeedbackDestroy = '/e-pusti/campaign-feedbacks/destroy'

// campaign material routes
export const campaignMatList = '/e-pusti/admin-campaign-material/list'
export const campaignMatStore = '/e-pusti/admin-campaign-material/store'
export const campaignMatUpdate = '/e-pusti/admin-campaign-material/update'
export const campaignMatDetailsStore = '/e-pusti/admin-campaign-material-details/store'
export const campaignMatDetailsDestroy = '/e-pusti/admin-campaign-material-details/destroy'
export const campaignMatDetailsList = '/e-pusti/admin-campaign-material-details/list'

// campaign dashboard
export const campaignDashboardList = '/e-pusti/campaign-dashboard/list'
