export const inputItems = [
    {
      id: 1,
      name: 'Auto Complete',
      icon: '<i class="ion-bowtie"></i>',
      field: `<b-form-group
                class="row text-black font-weight-bold"
                label-cols-sm="2"
                label="Label"
                label-for="label_id"
              >
              <b-form-input
                  id="label_id"
                  v-model="search.label_id"
                  placeholder="Auto Complete"
                  ></b-form-input>
              </b-form-group>`,
      status: 1
    },
    {
      id: 2,
      name: 'Button',
      icon: '<i class="fas fa-grip-horizontal"></i>',
      field: '',
      status: 1
    },
    {
      id: 2,
      name: 'Checkbox Group',
      icon: '<i class="fas fa-tasks"></i>',
      field: `<b-form-group
                class="row text-black font-weight-bold"
                label-cols-sm="2"
                label="Required"
                label-for="required_id"
              >
              <b-form-checkbox
                  id="required_id"
                  v-model="search.required_id"
                  ></b-form-checkbox>
              </b-form-group>`,
      status: 1
    },
    {
      id: 3,
      name: 'Date Field',
      icon: '<i class="far fa-calendar-alt"></i>',
      field: '',
      status: 1
    },
    {
      id: 4,
      name: 'File Upload',
      icon: '<i class="fas fa-upload"></i>',
      field: '',
      status: 1
    },
    {
      id: 5,
      name: 'Header',
      icon: '<b class="font-size-16">H</b>',
      field: '',
      status: 1
    },
    {
      id: 6,
      name: 'Hidden Input',
      icon: '<i class="far fa-keyboard"></i>',
      field: '',
      status: 1
    },
    {
      id: 7,
      name: 'Number',
      icon: '<i class="ion-pound"></i>',
      field: '',
      status: 1
    },
    {
      id: 8,
      name: 'Paragraph',
      icon: '<i class="fas fa-paragraph"></i>',
      field: '',
      status: 1
    },
    {
      id: 9,
      name: 'Radio Group',
      icon: '<i class="far fa-list-alt"></i>',
      field: '',
      status: 1
    },
    {
      id: 10,
      name: 'Select',
      icon: '<i class="fas fa-list-ol"></i>',
      field: '',
      status: 1
    },
    {
      id: 11,
      name: 'Text Field',
      icon: '<i class="far fa-square"></i>',
      field: '',
      status: 1
    },
    {
      id: 12,
      name: 'Text Area',
      icon: '<i class="far fa-square"></i>',
      field: '',
      status: 1
    }
  ]
