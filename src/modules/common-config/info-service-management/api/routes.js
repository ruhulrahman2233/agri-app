// Service Eligibility Type
export const serviceEligibilityList = 'service-eligibility-type/list'
export const serviceEligibilityStore = 'service-eligibility-type/store'
export const serviceEligibilityUpdate = 'service-eligibility-type/update'
export const serviceEligibilityToggleStatus = 'service-eligibility-type/toggle-status'
export const serviceEligibilityDelete = 'service-eligibility-type/destroy'
// manage FAQ api Routes

export const manageFaqstore = '/faq/store'
export const manageFaqList = '/faq/list'
export const manageFaqToggleStatus = '/faq/toggle-status'
export const manageFaqUpdate = '/faq/update'
// Content Entry
export const contentList = 'master-contents/list'
export const contentStore = 'master-contents/store'
export const contentUpdate = 'master-contents/update'
export const contentToggleStatus = 'master-contents/toggle-status'
export const contentDelete = 'master-contents/destroy'
