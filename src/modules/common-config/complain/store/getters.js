export default {
  testFind: (state) => (id) => state.testList.find(test => test.id === id),
  complainTypeFind: (state) => (id) => state.complainTypes.find(complainType => complainType.id === id),
  complainDesignationFind: (state) => (id) => state.complainDesignations.find(complainDesignation => complainDesignation.id === id),
  complainChainFind: (state) => (id) => state.complainChains.find(complainChain => complainChain.id === id),
  complainSendFind: (state) => (id) => state.complainSends.find(complainSend => complainSend.id === id)
}
