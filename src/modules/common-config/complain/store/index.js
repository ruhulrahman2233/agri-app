import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  complainTypes: [],
  complainInfos: [],
  complainDesignations: [],
  complainChains: [],
  complainRequestList: [],
  complainSends: []
}
export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
