// Test
export const test = '/test/list'
// Complain type start
export const complainTypeList = '/master-complain-type/list'
export const complainTypeStore = '/master-complain-type/store'
export const complainTypeUpdate = '/master-complain-type/update'
export const complainTypeToggleStatus = '/master-complain-type/toggle-status'
export const complainTypeDestroy = '/master-complain-type/destroy'
// Complain type end
// submit complain info start
export const complainInfoList = '/complain-info/list'
export const complainInfoStore = '/complain-info/store'
export const complainInfoUpdate = '/complain-info/update'
export const complainInfoToggleStatus = '/complain-info/toggle-status'
export const complainInfoSolveStatus = '/complain-info/solve-status'
export const complainInfoDestroy = '/complain-info/destroy'
// submit complain info end
// Complain Designation start
export const complainDesignationList = '/master-complain-designation/list'
export const complainDesignationStore = '/master-complain-designation/store'
export const complainDesignationUpdate = '/master-complain-designation/update'
export const complainDesignationToggleStatus = '/master-complain-designation/toggle-status'
export const complainDesignationDestroy = '/master-complain-designation/destroy'
// Complain Request
export const complainRequestList = '/complain-manage/complain-request'
export const assignCommitteeList = '/complain-manage/assign-committee'
export const getCommitteeList = '/complain-manage/get-committee'
export const complainReportSubmit = '/complain-manage/report-submit'
export const complainApproveSubmit = '/complain-manage/complain-approve'
export const complainRejectSubmit = '/complain-manage/complain-reject'
export const getCommitteeReport = '/complain-manage/get-committee-report'
// Complain Chain start
export const complainChainList = '/master-complain-chain/list'
export const complainChainStore = '/master-complain-chain/store'
export const complainChainUpdate = '/master-complain-chain/update'
export const complainChainToggleStatus = '/master-complain-chain/toggle-status'
export const complainChainDestroy = '/master-complain-chain/destroy'
// Complain Chain end
export const complainSendList = '/complain-send/list'
export const complainSendStore = '/complain-send/store'
export const complainSendUpdate = '/complain-send/update'
export const complainSendToggleStatus = '/complain-send/toggle-status'
export const complainSendDestroy = '/complain-send/destroy'
