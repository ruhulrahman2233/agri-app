import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  users: [],
  categories: [],
  documents: [],
  archives: [],
  shows: []
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
