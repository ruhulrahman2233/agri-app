export default {
  userFind: (state) => (id) => state.users.find(user => user.id === id),
  categoryDocumentFind: (state) => (id) => state.categories.find(category => category.id === id),
  DocumentFind: (state) => (id) => state.documents.find(document => document.id === id)
}
