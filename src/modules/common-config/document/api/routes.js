// document Category
export const documentCategorylist = 'document-category/list'
export const documentCategorystore = 'document-category/store'
export const documentCategoryupdate = 'document-category/update'
export const documentCategorytoggleStatus = 'document-category/toggle-status'
export const documentCategorydestroy = 'document-category/delete'
// Add Document
export const addDocumentlist = 'document/list'
export const addDocumentStore = 'document/store'
export const addDocumentUpdate = 'document/update'
export const addDocumenttoggleStatus = 'document/toggle-status'
export const addDocumentArchivetoggleStatus = 'document/archive-toggle-status'
export const addDocumentdestroy = 'document/delete'
// Documnet indexing
export const categoryIndexingList = 'document-indexing/list'
export const categoryIndexingShow = 'document-indexing/show'
// Document Archived
export const documentArchivedList = 'document-archived/list'
