// database backup
export const dbBackupApi = 'data-archive/database-backup'
export const dbDownloadApi = 'data-archive/download-backup-db'
export const dbBackupListApi = 'data-archive/db-backup-files'
export const dbBackupDeleteApi = 'data-archive/db-backup-delete'
