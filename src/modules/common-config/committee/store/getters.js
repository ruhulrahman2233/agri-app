export default {
    createCommitteeFind: (state) => (id) => state.createCommittees.find(createCommittee => createCommittee.id === id),
    manageAgendaFind: (state) => (id) => state.manageAgendas.find(manageAgenda => manageAgenda.id === id),
    mettingMinuteFind: (state) => (id) => state.meetingMinutes.find(meetingMinute => meetingMinute.id === id),
    manageExpenseFind: (state) => (id) => state.manageExpensess.find(manageExpense => manageExpense.id === id)
}
