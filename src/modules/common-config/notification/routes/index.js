const childRoute = (prop) => [
  {
    path: 'notification-type-list',
    name: 'notification-type-list',
    meta: { auth: true, name: 'Editable' },
    component: () => import('../pages/notification-type/List.vue')
  },
  {
    path: 'notification-setting-list',
    name: 'notification-setting-list',
    meta: { auth: true, name: 'Editable' },
    component: () => import('../pages/notification-setting/List.vue')
  },
  {
    path: 'notification-template-list',
    name: 'notification-template-list',
    meta: { auth: true, name: 'Editable' },
    component: () => import('../pages/notification-template/List.vue')
  },
  {
    path: 'circulate-notice-list',
    name: 'circulate-notice-list',
    meta: { auth: true, name: 'Editable' },
    component: () => import('../pages/circulate-notice/List.vue')
  }
]

const routes = [
  {
    path: '/notification',
    name: 'notification',
    component: () => import('../../../../layouts/VerticleLayout.vue'),
    meta: { auth: true },
    children: childRoute('notification')
  }
]

export default routes
