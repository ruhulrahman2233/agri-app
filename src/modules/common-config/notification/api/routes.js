// Test
export const test = '/test/list'
// notification-type
export const notificationList = 'master-notification-type/list'
export const notificationStore = 'master-notification-type/store'
export const notificationUpdate = 'master-notification-type/update'
export const notificationToggleStatus = 'master-notification-type/toggle-status'
export const notificationDelete = 'master-notification-type/destroy'
// notification-setting
export const notificationSettingList = 'master-notification-setting/list'
export const notificationSettingStore = 'master-notification-setting/store'
export const notificationSettingUpdate = 'master-notification-setting/update'
export const notificationSettingToggleStatus = 'master-notification-setting/toggle-status'
export const notificationSettingDelete = 'master-notification-setting/destroy'
// notification-template
export const notificationTemplateList = 'notification-template/list'
export const notificationTemplateStore = 'notification-template/store'
export const notificationTemplateUpdate = 'notification-template/update'
export const notificationTemplateToggleStatus = 'notification-template/toggle-status'
export const notificationTemplateDelete = 'notification-template/destroy'
// circulate-notice
export const circulateNoticeList = 'notification-circulate-notice/list'
export const circulateNoticeStore = 'notification-circulate-notice/store'
export const circulateNoticeUpdate = 'notification-circulate-notice/update'
export const circulateNoticeToggleStatus = 'notification-circulate-notice/toggle-status'
export const circulateNoticeDelete = 'notification-circulate-notice/destroy'
