export default {
  notificationTypeFind: (state) => (id) => state.notificationTypes.find(notificationType => notificationType.id === id),
  notificationSettingFind: (state) => (id) => state.notificationSettings.find(notificationSetting => notificationSetting.id === id),
  notificationTemplateFind: (state) => (id) => state.notificationTemplates.find(notificationTemplate => notificationTemplate.id === id),
  circulateNoticeFind: (state) => (id) => state.circulateNotices.find(circulateNotice => circulateNotice.id === id)
}
