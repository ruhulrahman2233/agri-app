const childRoutes = (prop) => [
  {
      path: 'dashboard',
      name: prop + '.dashboard',
      component: () => import('../pages/dashboard/index')
  }
]
const routes = [
  {
    path: '/incentive-grant-service',
    name: 'incentive_grant_service',
    component: () => import('@/layouts/IncentiveGrantLayout.vue'),
    children: childRoutes('incentive-grant-service'),
    meta: { auth: true }
  }
]

export default routes
