export const list = '/list'
export const store = '/store'
export const update = '/update'
export const toggleStatus = '/toggle-status'
export const destroy = '/delete'

const irrigationDashboardPrefix = '/incentive-grant-dashboard'
export const fetchScheme = irrigationDashboardPrefix + '/fetch/scheme'
export const fetchTotal = irrigationDashboardPrefix + '/fetch/total'
export const fetchComplain = irrigationDashboardPrefix + '/fetch/complain'
export const fetchVisitor = irrigationDashboardPrefix + '/fetch/visitor'
