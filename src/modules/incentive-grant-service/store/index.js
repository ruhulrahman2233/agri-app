import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  commonObj: {
    hasDropdownLoaded: false,
    perPage: 10,
    dateFormat: 'dd/mm/YYYY',
    timeFormat: 'h:m',
    loading: false,
    listReload: false,
    enlistedUniversityList: [],
    educationLevelList: [],
    grantList: [],
    itemList: [],
    millTypeList: [],
    applicationList: [],
    seasonSetupList: [],
    millInfoList: [],
    subsidyTypeList: [],
    subsidyList: [],
    cultivationMethodList: [],
    projectList: [],
    regionInfoList: [],
    villageList: [],
    agMaterialTypeList: [],
    agMaterialList: [],
    circularInfoList: [],
    barcMsApplicationNameList: [],
    cropList: [],
    publicationTypeList: [],
    committeeList: []
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
