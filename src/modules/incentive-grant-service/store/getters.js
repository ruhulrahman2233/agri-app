export default {
  userFind: (state) => (id) => state.users.find(user => user.id === id)
}
