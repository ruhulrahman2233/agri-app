export default {
  localizeIncentiveGrantDropdown ({ commit }, payload) {
    commit('localizeIncentiveGrantDropdown', payload)
  }
}
