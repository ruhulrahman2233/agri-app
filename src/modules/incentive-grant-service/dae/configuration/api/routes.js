// Village Setup
export const VillageSetupBase = '/dae/config/dae-ms-villages-info'
export const VillageSetupList = VillageSetupBase + '/list'
export const VillageSetupStatus = VillageSetupBase + '/toggle-status'
export const VillageSetupStore = VillageSetupBase + '/store'
export const VillageSetupUpdate = VillageSetupBase + '/update'
export const VillageSetupDestroy = VillageSetupBase + '/destroy'

// Season setup
export const seasonSetUpList = '/dae/config/season-setup/list'
export const seasonSetUpStore = '/dae/config/season-setup/store'
export const seasonSetUpUpdate = '/dae/config/season-setup/update'
export const seasonSetUpStatus = '/dae/config/season-setup/toggle-status'
export const seasonSetUpDestroy = '/dae/config/season-setup/destroy'

// Region Info
export const regionInfoList = '/dae/config/dae-region-info/list'
export const regionInfoStore = '/dae/config/dae-region-info/store'
export const regionInfoUpdate = '/dae/config/dae-region-info/update'
export const regionInfoStatus = '/dae/config/dae-region-info/toggle-status'
export const regionInfoDestroy = '/dae/config/dae-region-info/destroy'

// Agriculture MetarialSetup
export const aggriMaterialSetupList = '/dae/config/agricultural-material/list'
export const aggriMaterialSetupStore = '/dae/config/agricultural-material/store'
export const aggriMaterialSetupUpdate = '/dae/config/agricultural-material/update'
export const aggriMaterialSetupStatus = '/dae/config/agricultural-material/toggle-status'
export const aggriMaterialSetupDestroy = '/dae/config/agricultural-material/destroy'

// Agriculture Metarial Type Setup
export const aggriMaterialTypeSetupList = '/dae/config/agricultural-material-setup/list'
export const aggriMaterialTypeSetupStore = '/dae/config/agricultural-material-setup/store'
export const aggriMaterialTypeSetupUpdate = '/dae/config/agricultural-material-setup/update'
export const aggriMaterialTypeSetupStatus = '/dae/config/agricultural-material-setup/toggle-status'
export const aggriMaterialTypeSetupDestroy = '/dae/config/agricultural-material-setup/destroy'

// Grant Type Wise Crop Mapping
export const grantTypeCropNameList = '/dae/config/grant-type-crop-mapping/list'
export const grantTypeCropNameStore = '/dae/config/grant-type-crop-mapping/store'
export const grantTypeCropNameUpdate = '/dae/config/grant-type-crop-mapping/update'
export const grantTypeCropNameStatus = '/dae/config/grant-type-crop-mapping/toggle-status'
export const grantTypeCropNameDestroy = '/dae/config/grant-type-crop-mapping/destroy'

// Crop Wise Agricultural Material Mapping
export const cropWiseAgMaMapList = '/dae/config/ms-mat-mapp/list'
export const cropWiseAgMaMapStore = '/dae/config/ms-mat-mapp/store'
export const cropWiseAgMaMapUpdate = '/dae/config/ms-mat-mapp/update'
export const cropWiseAgMaMapStatus = '/dae/config/ms-mat-mapp/toggle-status'
export const cropWiseAgMaMapDestroy = '/dae/config/ms-mat-mapp/destroy'

// Grant Prerequsite checklist setup
export const grantPrerequsiteCheckList = '/dae/config/grant-prerequisites/list'
export const grantPrerequsiteCheckStore = '/dae/config/grant-prerequisites/store'
export const grantPrerequsiteCheckUpdate = '/dae/config/grant-prerequisites/update'
export const grantPrerequsiteCheckStatus = '/dae/config/grant-prerequisites/toggle-status'
export const grantPrerequsiteCheckDestroy = '/dae/config/grant-prerequisites/destroy'
export const grantTypeWiseCropList = '/dae/config/grant-prerequisites/grant-type-wise-crop-list'

// Block Setup Api Routes...
export const blockSetupList = '/dae/config/dae-ms-blocks/list'
export const blockSetUpStore = '/dae/config/dae-ms-blocks/store'
export const blockSetUpUpdate = '/dae/config/dae-ms-blocks/update'
export const blockSetUpStatus = '/dae/config/dae-ms-blocks/toggle-status'
export const blockSetUpDestroy = '/dae/config/dae-ms-blocks/destroy'
