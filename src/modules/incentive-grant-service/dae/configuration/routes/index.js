const childRoutes = (prop) => [
    {
      path: 'village-setup',
      name: prop + 'village_setup',
      meta: { auth: true, name: 'Editable' },
      component: () => import('../pages/village-setup/List.vue')
    },
    {
        path: 'season-setup',
        name: prop + 'season_setup',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/season-setup/List.vue')
    },
    {
        path: 'block-setup',
        name: prop + 'block_setup',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/block-setup/List.vue')
    },
    {
        path: 'region-info',
        name: prop + 'region_info',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/region-info/List.vue')
    },
    {
        path: 'agriculture-metarial-type-setup',
        name: prop + 'agriculture_metarial_type_setup',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/agriculture-metarial-type-setup/List.vue')
    },
    {
        path: 'agriculture-material-setup',
        name: prop + 'agriculture_material_setup',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/agriculture-material-setup/List.vue')
    },
    {
        path: 'grant-prerequisite-checklist-setup',
        name: prop + 'grant_prerequisite_checklist_setup',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/grant-prerequisite-checklist-setup/List.vue')
    },
    {
        path: 'grant-type-wise-crop-mapping',
        name: prop + 'grant_type_wise_crop_mapping',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/grant-type-wise-crop-mapping/List.vue')
    },
    {
        path: 'crop-wise-agricultural-material-mapping',
        name: prop + 'crop_wise_agricultural_material_mapping',
        meta: { auth: true, name: 'Editable' },
        component: () => import('../pages/crop-wise-agricultural-material-mapping/List.vue')
    }
]

const routes = [
    {
        path: '/incentive-grant-service/dae/configuration',
        name: 'incentive_grant_service.dae.configuration',
        component: () => import('@/layouts/IncentiveGrantLayout.vue'),
        meta: { auth: true },
        children: childRoutes('incentive_grant_service.dae.configuration')
    }
]

export default routes
