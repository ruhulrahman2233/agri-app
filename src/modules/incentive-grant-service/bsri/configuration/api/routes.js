// Mill Information
export const MillInformationBase = '/bsri/config/bsri-ms-mill-infos'
export const MillInformationList = MillInformationBase + '/list'
export const MillInformationStatus = MillInformationBase + '/toggle-status'
export const MillInformationStore = MillInformationBase + '/store'
export const MillInformationUpdate = MillInformationBase + '/update'
export const MillInformationDestroy = MillInformationBase + '/destroy'

// mill Type
export const MillTypeBase = '/bsri/config/mill-types'
export const millTypeList = MillTypeBase + '/list'
export const millTypeStore = MillTypeBase + '/store'
export const millTypeShow = MillTypeBase + '/show'
export const millTypeUpdate = MillTypeBase + '/update'
export const millTypeStatus = MillTypeBase + '/toggle-status'
export const millTypeDestroy = MillTypeBase + '/destroy'
// Subsidy Type Setup
export const subsidyTypeBase = '/bsri/config/subsidy-types'
export const subsidyTypeList = subsidyTypeBase + '/list'
export const subsidyTypeStore = subsidyTypeBase + '/store'
export const subsidyTypeShow = subsidyTypeBase + '/show'
export const subsidyTypeUpdate = subsidyTypeBase + '/update'
export const subsidyTypeStatus = subsidyTypeBase + '/toggle-status'
export const subsidyTypeDestroy = subsidyTypeBase + '/destroy'
// crop name
export const cropNameBase = '/bsri/config/crops'
export const cropnameList = cropNameBase + '/list'
export const cropnameStore = cropNameBase + '/store'
export const cropnameShow = cropNameBase + '/show'
export const cropnameUpdate = cropNameBase + '/update'
export const cropnameStatus = cropNameBase + '/toggle-status'
export const cropnameDestroy = cropNameBase + '/destroy'

// cultivation method setup
export const cultivationMethods = '/bsri/config/cultivation-methods'
export const cultiMethodSetupList = cultivationMethods + '/list'
export const cultiMethodSetupStore = cultivationMethods + '/store'
export const cultiMethodSetupShow = cultivationMethods + '/show'
export const cultiMethodSetupUpdate = cultivationMethods + '/update'
export const cultiMethodSetupStatus = cultivationMethods + '/toggle-status'
export const cultiMethodSetupDestroy = cultivationMethods + '/destroy'
// Subsidy Setup
export const subsidySetupBase = '/bsri/config/subsidies'
export const subsidySetupList = subsidySetupBase + '/list'
export const subsidySetupStore = subsidySetupBase + '/store'
export const subsidySetupShow = subsidySetupBase + '/show'
export const subsidySetupUpdate = subsidySetupBase + '/update'
export const subsidySetupStatus = subsidySetupBase + '/toggle-status'
export const subsidySetupDestroy = subsidySetupBase + '/destroy'
// Subsidy Policy Setup
export const SubsidyPolicySetupBase = '/bsri/config/subsidy-policies'
export const SubsidyPolicySetupList = SubsidyPolicySetupBase + '/list'
export const SubsidyPolicySetupStatus = SubsidyPolicySetupBase + '/toggle-status'
export const SubsidyPolicySetupStore = SubsidyPolicySetupBase + '/store'
export const SubsidyPolicySetupUpdate = SubsidyPolicySetupBase + '/update'
export const SubsidyPolicySetupDestroy = SubsidyPolicySetupBase + '/destroy'

// cultivation method wise crop mapping
export const methodsWiseCropMap = '/bsri/config/cultivation-method-crop-mapping'
export const methodWiseCropList = methodsWiseCropMap + '/list'
export const methodWiseCropStore = methodsWiseCropMap + '/store'
export const methodWiseCropShow = methodsWiseCropMap + '/show'
export const methodWiseCropUpdate = methodsWiseCropMap + '/update'
export const methodWiseCropStatus = methodsWiseCropMap + '/toggle-status'
export const methodWiseCropDestroy = methodsWiseCropMap + '/destroy'

// project setup
export const projectSetup = '/bsri/config/projects'
export const projectSetupList = projectSetup + '/list'
export const projectSetupStore = projectSetup + '/store'
export const projectSetupShow = projectSetup + '/show'
export const projectSetupUpdate = projectSetup + '/update'
export const projectSetupStatus = projectSetup + '/toggle-status'
export const projectSetupDestroy = projectSetup + '/destroy'

// BSRI method setup
export const bsriRWMMethods = '/bsri/config/bsri-region-wise-mill-mappin'
export const rwmMapingList = bsriRWMMethods + '/list'
export const rwmMapingStore = bsriRWMMethods + '/store'
export const rwmMapingShow = bsriRWMMethods + '/show'
export const rwmMapingUpdate = bsriRWMMethods + '/update'
export const rwmMapingStatus = bsriRWMMethods + '/toggle-status'
export const rwmMapingDestroy = bsriRWMMethods + '/destroy'

// BSRI project subsidy mapping
export const projectSubsidy = '/bsri/config/project-subsidy'
export const projectSubsidyList = projectSubsidy + '/list'
export const projectSubsidyStore = projectSubsidy + '/store'
export const projectSubsidyShow = projectSubsidy + '/show'
export const projectSubsidyUpdate = projectSubsidy + '/update'
export const projectSubsidyStatus = projectSubsidy + '/toggle-status'
export const projectSubsidyDestroy = projectSubsidy + '/destroy'
