export const mutations = {
  mutateTrainingElearningCommonProperties (state, payload) {
    state.commonObj = Object.assign({}, state.commonObj, payload)
  },
  localizeCommonDropdownTrainingElearningService (state, payload) {
    state.commonObj.trainingTypeList = state.commonObj.trainingTypeList.map(item => {
      const tmp = payload.value === 'en' ? { text: item.text_en } : { text: item.text_bn }
      return Object.assign({}, item, tmp)
    })
    state.commonObj.trainingCategoryList = state.commonObj.trainingCategoryList.map(item => {
      const tmp = payload.value === 'en' ? { text: item.text_en } : { text: item.text_bn }
      return Object.assign({}, item, tmp)
    })

    state.commonObj.trainingTitleList = state.commonObj.trainingTitleList.map(item => {
      const tmp = payload.value === 'en' ? { text: item.text_en } : { text: item.text_bn }
      return Object.assign({}, item, tmp)
    })

    state.commonObj.roomTypeList = state.commonObj.roomTypeList.map(item => {
      const tmp = payload.value === 'en' ? { text: item.text_en } : { text: item.text_bn }
      return Object.assign({}, item, tmp)
    })

    state.commonObj.courseDocumentSetupList = state.commonObj.courseDocumentSetupList.map(item => {
      const tmp = payload.value === 'en' ? { text: item.text_en } : { text: item.text_bn }
      return Object.assign({}, item, tmp)
    })

    state.commonObj.accomodationSetupList = state.commonObj.accomodationSetupList.map(item => {
      const tmp = payload.value === 'en' ? { text: item.text_en } : { text: item.text_bn }
      return Object.assign({}, item, tmp)
    })
  }
}
