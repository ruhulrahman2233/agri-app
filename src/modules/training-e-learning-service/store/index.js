import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  commonObj: {
    hasDropdownLoaded: false,
    perPage: 10,
    dateFormat: 'dd/mm/YYYY',
    timeFormat: 'h:m',
    loading: false,
    listReload: false,
    trainingTypeList: [],
    trainingCategoryList: [],
    trainingTitleList: [],
    roomTypeList: [],
    accomodationSetupList: [],
    courseDocumentSetupList: []
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
