// Training Type api
export const trainingTypeBaseUrl = '/config/master-training-types/'
export const trainingTypeList = trainingTypeBaseUrl + 'list'
export const trainingTypeToggleStatus = trainingTypeBaseUrl + 'toggle-status'
export const trainingTypeStore = trainingTypeBaseUrl + 'store'
export const trainingTypeUpdate = trainingTypeBaseUrl + 'update'
export const trainingTypeDestroy = trainingTypeBaseUrl + 'destroy'
// Training Categry api
export const trainingCategoryBaseUrl = '/config/master-training-category/'
export const trainingCategoryList = trainingCategoryBaseUrl + 'list'
export const trainingCategoryToggleStatus = trainingCategoryBaseUrl + 'toggle-status'
export const trainingCategoryStore = trainingCategoryBaseUrl + 'store'
export const trainingCategoryUpdate = trainingCategoryBaseUrl + 'update'
export const trainingCategoryDestroy = trainingCategoryBaseUrl + 'destroy'

// Training Title api
export const trainingTitleList = '/config/training-title/list'
export const trainingTitleStore = '/config/training-title/store'
export const trainingTitleUpdate = '/config/training-title/update'
export const trainingTitleToggleStatus = '/config/training-title/toggle-status'
export const trainingTitleDestroy = '/config/training-title/destroy'

// Room Type api
export const roomTypeList = '/config/room-types/list'
export const roomTypeStore = '/config/room-types/store'
export const roomTypeUpdate = '/config/room-types/update'
export const roomTypeToggleStatus = '/config/room-types/toggle-status'
export const roomTypeDestroy = '/config/room-types/destroy'

// Room Rent type api
export const roomRentList = '/config/room-rents/list'
export const roomRentStore = '/config/room-rents/store'
export const roomRentUpdate = '/config/room-rents/update'
export const roomRentToggleStatus = '/config/room-rents/toggle-status'
export const roomRentDestroy = '/config/room-rents/destroy'

// Pay Grade Setup api
export const payGradeSetupList = '/config/pay-grade-setup/list'
export const payGradeSetupStore = '/config/pay-grade-setup/store'
export const payGradeSetupUpdate = '/config/pay-grade-setup/update'
export const payGradeSetupToggleStatus = '/config/pay-grade-setup/toggle-status'
export const payGradeSetupDestroy = '/config/pay-grade-setup/destroy'

// Accomodation Setup api
export const accomodationSetupBaseUrl = '/config/master-accomodation-setup/'
export const accomodationSetupList = accomodationSetupBaseUrl + 'list'
export const accomodationSetupToggleStatus = accomodationSetupBaseUrl + 'toggle-status'
export const accomodationSetupStore = accomodationSetupBaseUrl + 'store'
export const accomodationSetupUpdate = accomodationSetupBaseUrl + 'update'
export const accomodationSetupDestroy = accomodationSetupBaseUrl + 'destroy'

// Course Document Setup api
export const courseDocumentSetupBaseUrl = '/config/master-course-document-setup/'
export const courseDocumentSetupList = courseDocumentSetupBaseUrl + 'list'
export const courseDocumentSetupToggleStatus = courseDocumentSetupBaseUrl + 'toggle-status'
export const courseDocumentSetupStore = courseDocumentSetupBaseUrl + 'store'
export const courseDocumentSetupUpdate = courseDocumentSetupBaseUrl + 'update'
export const courseDocumentSetupDestroy = courseDocumentSetupBaseUrl + 'destroy'
