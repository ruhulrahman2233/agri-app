const childRoutes = (prop) => [
    {
      path: 'training-type',
      name: prop + 'training_type',
      meta: { auth: true },
      component: () => import('../pages/training-type/List.vue')
    },
    {
      path: 'training-category',
      name: prop + 'training_category',
      meta: { auth: true },
      component: () => import('../pages/training-category/List.vue')
    },
    {
      path: 'training-title',
      name: prop + 'training_title',
      meta: { auth: true },
      component: () => import('../pages/training-title/List.vue')
    },
    {
      path: 'room-type',
      name: prop + 'room_type',
      meta: { auth: true },
      component: () => import('../pages/room-type/List.vue')
    },
    {
      path: 'room-rent',
      name: prop + 'room_rent',
      meta: { auth: true },
      component: () => import('../pages/room-rent/List.vue')
    },
    {
      path: 'pay-grade-setup',
      name: prop + 'pay_grade_setup',
      meta: { auth: true },
      component: () => import('../pages/pay-grade-setup/List.vue')
    },
    {
      path: 'course-document-setup',
      name: prop + 'course_document_setup',
      meta: { auth: true },
      component: () => import('../pages/course-document-setup/List.vue')
    },
    {
      path: 'accomodation-setup',
      name: prop + 'accomodation_setup',
      meta: { auth: true },
      component: () => import('../pages/accomodation-setup/List.vue')
    }
]

const routes = [
    {
      path: '/training-e-learning-service/configuration',
      name: 'training_e_learning_service.configuration',
      component: () => import('@/layouts/TrainingElearningLayout.vue'),
      meta: { auth: true },
      children: childRoutes('training_e_service.barc.configuration')
    }
]

export default routes
