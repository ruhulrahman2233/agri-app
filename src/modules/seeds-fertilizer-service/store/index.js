import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import Custom from './custom'

/**
 * This store will be used for all modules of this component
 */
const state = {
  commonObj: {
    hasDropdownLoaded: false,
    programTypeList: [],
    carryoverList: [],
    portZoneSetupList: [],
    CropTypeList: [],
    CropNameList: [],
    cnfAgentList: [],
    transportAgentList: [],
    fertilizerImportCountryList: [],
    salesCenterList: [],
    VarietyList: [],
    seedClassList: [],
    productionSourceList: [],
    measurementUnitList: [],
    productionSeasonList: [],
    fertilizerTypeList: [],
    fertilizerNameList: [],
    portInfoSetupList: [],
    godownInfoList: [],
    allocationTypeList: [],
    organizationList: [],
    priceTypeList: [],
    packetVarietyList: [],
    zoneOfficeList: [],
    spcOfficeList: [],
    prerequisiteChecklist: [],
    gpOrganizationsList: [],
    dealerBasicList: [],
    gpCropNameList: [],
    gpCropTypeList: [],
    gpConservationTypeList: [],
    gpUnitList: [],
    collectionSourceList: [],
    characterizationTypeList: [],
    germplasmRequestList: [],
    descriptorLabelList: [],
    descriptorHeadingList: [],
    codeGuideDetail: [],
    dealerBasicInfoList: [],
    monthList: Custom.monthList
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
