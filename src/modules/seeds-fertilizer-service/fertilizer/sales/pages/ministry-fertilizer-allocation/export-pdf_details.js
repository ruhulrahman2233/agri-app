import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'
import { dateFormat } from '@/Utils/fliter'

const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, data, vm, fertilizer, district) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
        const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
        const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
        const pdfContent = [
          {
              columns: reportHeadData.reportHeadColumn
          },
          {
            text: vm.$t('fertilizerReport.krishi_bhaban'),
            style: 'krishi',
            alignment: 'center'
          },
          { text: reportHeadData.address, style: 'header3', alignment: 'center' }
        ]

        if (reportHeadData.projectName) {
          pdfContent.push({ text: reportHeadData.projectName, style: 'header3', alignment: 'center' })
        }

        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        pdfContent.push(
          {
            table: {
              style: 'tableData',
              headerRows: 0,
              widths: ['*', '*', '*', '*'],
              body: [
                [
                  { text: vm.$t('globalTrans.fiscalYear'), style: 'th' },
                  { text: vm.$i18n.locale === 'bn' ? data.fiscal_year_bn : data.fiscal_year, style: 'td' },
                  { text: vm.$t('fertilizerSales.month'), style: 'th' },
                  { text: vm.$i18n.locale === 'bn' ? data.month_bn : data.month_en, style: 'td' }
                ],
                [
                  { text: vm.$t('movement.allocationDate'), style: 'th' },
                  { text: dateFormat(data.allocation_date), style: 'td' },
                  { text: vm.$t('fertilizerSales.allocationType'), style: 'th' },
                  { text: (i18n.locale === 'en') ? data.allocation : data.allocation_bn, style: 'td' }
                ],
                [
                  { text: vm.$t('fertilizerSales.comment'), style: 'th' },
                  { text: vm.$i18n.locale === 'en' ? data.comments : data.comments_bn, style: 'td' },
                  { text: '', style: 'td' },
                  { text: '', style: 'td' }
                ]
              ]
            }
          }
        )
        pdfContent.push({ text: '', style: 'fertilizer', alignment: 'center' })
        var tableRows = []
        var tabledata1 = [
            { text: vm.$t('globalTrans.sl_no'), rowSpan: 2, style: 'th', alignment: 'center' },
            { text: vm.$t('movement.godownNameDistrict'), rowSpan: 2, colSpan: 2, style: 'th', alignment: 'center' },
            {},
            { text: vm.$t('movement.fertilizerName') + ' [' + vm.$t('fertilizerSales.m_ton') + ']', colSpan: 3, style: 'th', alignment: 'center' },
            {},
            {}
          ]
        tableRows.push(tabledata1)

        var tabledata = [{}, {}, {}]
        fertilizer.map(doc => {
            const txt = { text: vm.$i18n.locale === 'en' ? doc.text_en : doc.text_bn, style: 'th', alignment: 'center' }
            tabledata.push(txt)
        })
        tableRows.push(tabledata)

        district.map((district, distId) => {
            var dist = [
                { text: vm.$n(distId + 1), style: 'td', alignment: 'center' },
                { text: vm.$i18n.locale === 'en' ? district.text_en : district.text_bn, colSpan: 2, style: 'td', alignment: 'center' },
                {}
              ]
            fertilizer.map((fertilizer, ferId) => {
                const txt = { text: vm.$n(data.allocation_details[distId][ferId].amount), style: 'td', alignment: 'center' }
                dist.push(txt)
            })
            tableRows.push(dist)
        })

        var tablefooter = [
            { text: vm.$t('movement.total'), colSpan: 3, style: 'td', alignment: 'right', bold: true },
            {},
            {}
          ]
        fertilizer.map((doc, index) => {
            const txt = { text: vm.$n(data.total[index]), style: 'td', alignment: 'center', bold: true }
            tablefooter.push(txt)
        })
        tableRows.push(tablefooter)

        var tablefooter1 = [
            { text: '', colSpan: 3, style: 'td', bold: true },
            {},
            {}
          ]
        fertilizer.map(doc => {
            const txt = { text: vm.$t('movement.mTon'), style: 'td', alignment: 'center', bold: true }
            tablefooter1.push(txt)
        })
        tableRows.push(tablefooter1)
        pdfContent.push(
            {
              table: {
                  headerRows: 0,
                  style: 'header2',
                  widths: '*',
                  body: tableRows
              }
            }
        )
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'Portrait',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
          th: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          td: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          header: {
            fontSize: 12,
            margin: [0, 0, 0, 4]
          },
          header2: {
            fontSize: 14,
            margin: [0, 10, 0, 20]
          },
          fertilizer: {
            fontSize: 10,
            margin: [10, 10, 0, 20]
          },
          headerPort1: {
            fontSize: 10,
            margin: [0, 20, 0, 0]
          },
          headerPort: {
            fontSize: 10,
            margin: [0, 4, 0, 15]
          },
          krishi: {
            margin: [0, -5, 0, 15],
            alignment: 'center'
          },
          header3: {
            fontSize: 9,
            margin: [0, 0, 0, 4]
          },
          tableSubHead: {
            margin: [0, 5, 0, 15]
          }
        }
      }
      pdfMake.createPdf(docDefinition, null, null, null).download('ministry-fertilizer-allocation')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
