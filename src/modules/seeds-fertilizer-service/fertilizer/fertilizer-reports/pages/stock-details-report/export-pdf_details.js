import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'
import { dateFormat } from '@/Utils/fliter'
// function getMonth (month) {
//     month = parseInt(month)
//     if (month === 1) {
//         return i18n.locale === 'bn' ? 'জুলাই' : 'July'
//     } else if (month === 2) {
//         return i18n.locale === 'bn' ? 'আগস্ট' : 'August'
//     } else if (month === 3) {
//         return i18n.locale === 'bn' ? 'সেপ্টেম্বর' : 'September'
//     } else if (month === 4) {
//         return i18n.locale === 'bn' ? 'অক্টোবর' : 'October'
//     } else if (month === 5) {
//         return i18n.locale === 'bn' ? 'নভেম্বর' : 'November'
//     } else if (month === 6) {
//         return i18n.locale === 'bn' ? 'ডিসেম্বর' : 'December'
//     } else if (month === 7) {
//         return i18n.locale === 'bn' ? 'জানুয়ারী' : 'January'
//     } else if (month === 8) {
//         return i18n.locale === 'bn' ? 'ফেব্রুয়ারী' : 'February'
//     } else if (month === 9) {
//         return i18n.locale === 'bn' ? 'মার্চ' : 'March'
//     } else if (month === 10) {
//         return i18n.locale === 'bn' ? 'এপ্রিল' : 'April'
//     } else if (month === 11) {
//         return i18n.locale === 'bn' ? 'মে' : 'May'
//     } else if (month === 12) {
//         return i18n.locale === 'bn' ? 'জুন' : 'June'
//     }
//   }
const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, data, vm, search) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
      }
      const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
      const pdfContent = [
          {
              columns: reportHeadData.reportHeadColumn, style: 'main_head'
          },
          {
            text: vm.$t('fertilizerReport.krishi_bhaban'),
            style: 'krishi',
            alignment: 'center'
          },
          { text: reportHeadData.address, style: 'address', alignment: 'center' }
        ]
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center' })
        const allRowsHead = [
          [
            { text: vm.$t('fertilizerReport.fertilizer'), style: 'td', alignment: 'right' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? search.fertilizer_name_bn : search.fertilizer_name, style: 'td', alignment: 'left' },
            { text: vm.$t('globalTrans.from_date'), style: 'td', alignment: 'right' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: dateFormat(search.date_from), style: 'td', alignment: 'left' },
            { text: vm.$t('globalTrans.to_date'), style: 'td', alignment: 'right' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: dateFormat(search.date_to), style: 'td', alignment: 'left' }
          ],
          [
            { text: vm.$t('fertilizerReport.godown'), style: 'td', alignment: 'right' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? search.godown_name_bn : search.godown_name, style: 'td', alignment: 'left' },
            { text: vm.$t('fertilizerReport.sale_center_name'), style: 'td', alignment: 'right' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? search.sale_center_bn : search.sale_center, style: 'td', alignment: 'left' },
            { text: vm.$t('fertilizerReport.region_name'), style: 'td', alignment: 'right' },
            { text: ':', style: 'td', alignment: 'center' },
            { text: (i18n.locale === 'bn') ? search.region_name_bn : search.region_name, style: 'td', alignment: 'left' }
          ]
        ]
        pdfContent.push({
          table: {
            headerRows: 1,
            widths: ['15%', '2%', '13%', '15%', '2%', '18%', '15%', '2%', '18%'],
            body: allRowsHead
          },
          layout: {
            hLineWidth: function (i, node) {
              return 0
            },
            vLineWidth: function (i, node) {
              return 0
            }
          }
        })
        pdfContent.push({ text: '', style: 'fertilizer', alignment: 'center' })
        // pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        const allRows = [
          [
            { text: vm.$t('fertilizerReport.date'), style: 'th', rowSpan: 2, alignment: 'center' },
            { text: vm.$t('fertilizerReport.opening_stock'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.receive_details'), style: 'th', rowSpan: 2, alignment: 'center' },
            { text: vm.$t('fertilizerReport.challan_fertilizer_amount'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.fertilizer_receive_amount'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.difference_amount'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.sale_details'), style: 'th', rowSpan: 2, alignment: 'center' },
            { text: vm.$t('fertilizerReport.sale_distribution'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.sent_elsewhere'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.final_stock'), style: 'th', colSpan: 2, alignment: 'center' },
            {},
            { text: vm.$t('fertilizerReport.comments'), style: 'th', rowSpan: 2, alignment: 'center' }
          ],
          [
            {},
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            {},
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            {},
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.bag'), style: 'th1', alignment: 'right' },
            { text: vm.$t('fertilizerReport.m_ton'), style: 'th1', alignment: 'right' },
            {}
          ]
        ]
        data.map((report, index) => {
          allRows.push(
          [
            { text: dateFormat(report.date), style: 'td', alignment: 'center' },
            { text: vm.$n(report.opening * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.opening), style: 'td', alignment: 'right' },
            { text: vm.$t('fertilizerReport.according_to_received_report'), style: 'td', alignment: 'center' },
            { text: vm.$n(report.delivered_total * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.delivered_total), style: 'td', alignment: 'right' },
            { text: vm.$n(report.received_total * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.received_total), style: 'td', alignment: 'right' },
            { text: vm.$n(report.difference_total * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.difference_total), style: 'td', alignment: 'right' },
            { text: vm.$t('fertilizerReport.according_to_sales_report'), style: 'td', alignment: 'center' },
            { text: vm.$n(report.sold * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.sold), style: 'td', alignment: 'right' },
            { text: vm.$n(report.inter_delivered_from * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.inter_delivered_from), style: 'td', alignment: 'right' },
            { text: vm.$n(report.final_stock * 20), style: 'td', alignment: 'right' },
            { text: vm.$n(report.final_stock), style: 'td', alignment: 'right' },
            {}
          ])
        })
        pdfContent.push({
          table: {
            headerRows: 2,
            widths: ['7%', '5%', '5%', '8%', '5%', '5%', '5%', '5%', '5%', '5%', '8%', '5%', '5%', '5%', '5%', '5%', '5%', '7%'],
            body: allRows
          }
        })
        const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'landscape',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
            th: {
              fontSize: 10,
              margin: [3, 3, 3, 3]
            },
            th1: {
                fontSize: 9
              },
            td: {
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            search: {
              fontSize: 8,
              margin: [3, 3, 3, 3]
            },
            fertilizer: {
              margin: [0, 10, 0, 7]
            },
            fertilizerSHeader: {
                fontSize: 10,
                margin: [40, 0, 0, 0]
            },
            header: {
              fontSize: 12,
              margin: [0, 0, 0, 4]
            },
            header2: {
              fontSize: 10,
              margin: [0, 10, 0, 20]
            },
            headerPort1: {
              fontSize: 10,
              margin: [0, 20, 0, 5]
            },
            headerPort: {
              fontSize: 10,
              margin: [0, 4, 0, 15]
            },
            krishi: {
              margin: [0, 1, 0, 15],
              alignment: 'center'
            },
            header3: {
              fontSize: 9,
              margin: [0, 15, 0, 0]
            },
            address: {
              fontSize: 9,
              margin: [0, -10, 0, 0]
            },
            tableSubHead: {
              margin: [0, 5, 0, 15]
            }
          }
        }
        pdfMake.createPdf(docDefinition, null, null, null).download('stock-details-report')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
