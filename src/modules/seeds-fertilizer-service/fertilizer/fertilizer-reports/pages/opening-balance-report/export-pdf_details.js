import i18n from '@/i18n'
import Store from '@/store'
import ReportHeading from '@/Utils/report-head'
import pdfMake from 'pdfmake/build/pdfmake'
import pdfFontsBn from 'pdfmake/build/vfs_fonts_bn'
import pdfFontsEn from 'pdfmake/build/vfs_fonts_en'

const exportPdfDetails = async (baseUrl, uri = '/report-heading/detail', orgId, reportTitle, data, vm, typeName = '', tspTotal, mopTotal, dapTotal, total) => {
    try {
      Store.commit('mutateCommonProperties', {
        loading: true
      })
      if (i18n.locale === 'bn') {
        pdfMake.vfs = pdfFontsBn.pdfMake.vfs
      } else {
          pdfMake.vfs = pdfFontsEn.pdfMake.vfs
        }
        const reportHeadData = await ReportHeading.getReportHead(baseUrl, uri, orgId)
        const pdfContent = [
          {
              columns: reportHeadData.reportHeadColumn, style: 'main_head'
          },
          {
            text: vm.$t('fertilizerReport.krishi_bhaban'),
            style: 'krishi',
            alignment: 'center'
          },
          { text: reportHeadData.address, style: 'address', alignment: 'center' }
        ]

        if (reportHeadData.projectName) {
          pdfContent.push({ text: reportHeadData.projectName, style: 'header3', alignment: 'center' })
        }
        pdfContent.push({ text: reportTitle, style: 'header2', alignment: 'center', decoration: 'underline' })
        if (typeName) {
          pdfContent.push({ text: typeName, style: 'report_type', alignment: 'center' })
        }
        pdfContent.push({ text: vm.$t('fertilizerReport.accountMayTons'), style: 'fertilizer', alignment: 'right', bold: true })
        const allRows = [
          [
            { text: vm.$t('globalTrans.sl_no'), style: 'th', alignment: 'center' },
            { text: vm.$t('globalTrans.nong'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.godown'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.TSP'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.MOP'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.DAP'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.total'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.name_of_district'), style: 'th', alignment: 'center' },
            { text: vm.$t('fertilizerReport.comments'), style: 'th' }
          ]
        ]
        data.forEach((info, index) => {
          allRows.push([
            { text: (i18n.locale === 'bn') ? info.office_name_bn : info.office_name, style: 'th', colSpan: 9, alignment: 'center' }
          ])
          info.goDownInfo.forEach((goDownInfo, index1) => {
            allRows.push([
              { text: vm.$n(goDownInfo.slNew), alignment: 'center', style: 'td' },
              { text: vm.$n(index1 + 1), alignment: 'center', style: 'td' },
              { text: (i18n.locale === 'bn') ? goDownInfo.name_bn : goDownInfo.name, alignment: 'center', style: 'td' },
              { text: vm.$n(goDownInfo.tspTotal), alignment: 'right', style: 'td' },
              { text: vm.$n(goDownInfo.mopTotal), alignment: 'right', style: 'td' },
              { text: vm.$n(goDownInfo.dapTotal), alignment: 'right', style: 'td' },
              { text: vm.$n(goDownInfo.total), alignment: 'right', style: 'td' },
              { text: (i18n.locale === 'bn') ? goDownInfo.district_name_bn : goDownInfo.district_name, alignment: 'center', style: 'td' },
              { text: '', alignment: 'center' }
            ])
          })
          allRows.push([
            { text: (i18n.locale === 'bn') ? info.office_name_bn + ' ' + vm.$t('fertilizerReport.total') : info.office_name + ' ' + vm.$t('fertilizerReport.total'), alignment: 'center', style: 'th', colSpan: 3, bold: true },
            {},
            {},
            { text: vm.$n(info.tsp), alignment: 'right', style: 'th', bold: true },
            { text: vm.$n(info.mop), alignment: 'right', style: 'th', bold: true },
            { text: vm.$n(info.dap), alignment: 'right', style: 'th', bold: true },
            { text: vm.$n(info.total), alignment: 'right', style: 'th', bold: true },
            { text: '', alignment: 'center', style: 'th', bold: true },
            { text: '', alignment: 'center', style: 'th', bold: true }
          ])
        })
        allRows.push([
          { text: vm.$t('fertilizerReport.GrandTotalMTon'), alignment: 'right', style: 'th', colSpan: 3, bold: true },
          {},
          {},
          { text: vm.$n(tspTotal), alignment: 'right', style: 'th', bold: true },
          { text: vm.$n(mopTotal), alignment: 'right', style: 'th', bold: true },
          { text: vm.$n(dapTotal), alignment: 'right', style: 'th', bold: true },
          { text: vm.$n(total), alignment: 'center', style: 'th', bold: true },
          { text: '', alignment: 'center', style: 'th', colSpan: 2, bold: true },
          {}
        ])
        const waterMarkText = i18n.locale === 'bn' ? 'কৃষি মন্ত্রণালয়' : 'Ministry of Agriculture'
        pdfContent.push({
          table: {
            headerRows: 1,
            widths: ['7%', '5%', '15%', '10%', '10%', '10%', '10%', '20%', '13%'],
            body: allRows
          }
        })

        var docDefinition = {
        content: pdfContent,
        pageSize: 'A4',
        pageOrientation: 'landscape',
        watermark: { text: waterMarkText, color: 'blue', opacity: 0.1, bold: true, italics: false },
        styles: {
          th: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          td: {
            fontSize: (i18n === 'bn') ? 12 : 10,
            margin: [3, 3, 3, 3]
          },
          header: {
            fontSize: 12,
            margin: [0, 0, 0, 4]
          },
          header2: {
            fontSize: 14,
            margin: [0, 10, 0, 20]
          },
          header3: {
            fontSize: 9,
            margin: [0, 0, 0, 0]
          },
          address: {
            fontSize: 9,
            margin: [0, -10, 0, 0]
          },
          tableSubHead: {
            margin: [0, 5, 0, 15]
          },
          krishi: {
            margin: [0, -5, 0, 15],
            alignment: 'center'
          },
          report_type: {
            fontSize: 9,
            margin: [0, 2, 0, 15]
          }
        }
      }
      pdfMake.createPdf(docDefinition, null, null, null).download('Initial stocks of region-based fertilizers')
    } catch (error) {
      if (error) {}
    }
    Store.commit('mutateCommonProperties', {
      loading: false
    })
}
export default {
  exportPdfDetails
}
