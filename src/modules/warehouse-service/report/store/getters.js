export default {
  userFind: state => state.tests,
  testFind: state => (id) => state.tests.find(item => item.id === id)
}
