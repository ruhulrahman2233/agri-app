import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  tests: [],
  farmerRequests: [],
  registrations: [],
  deliveryScheduleLists: [],
  recommendations: [],
  regdetails: [],
  paymentCollections: []
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
