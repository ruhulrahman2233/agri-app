export default {
  userFind: state => state.tests,
  testFind: state => (id) => state.tests.find(item => item.id === id),
  serviceFind: state => (id) => state.warehouseServices.find(warehouseService => warehouseService.id === id),
  bankLoanFacilityFind: state => (id) => state.warehouseBankLoanFacilitys.find(warehouseBankLoanFacility => warehouseBankLoanFacility.id === id)
}
