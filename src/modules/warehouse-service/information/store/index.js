import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  tests: [],
  warehouseServices: [],
  warehouseBankLoanFacilitys: []
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
