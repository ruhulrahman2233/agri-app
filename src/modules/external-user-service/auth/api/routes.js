export const userTypeListApi = '/external/user/user-type-list'
export const userDataApi = '/external/user'
export const signUp = '/external/user/sign-up'
export const usernameCheckApi = '/external/user/check-username'
