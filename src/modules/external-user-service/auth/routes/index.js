const childRoute = (prop) => [
  {
    path: 'sign-in',
    name: prop + '.sign_in',
    meta: { auth: false },
    component: () => import('../pages/Sign-in.vue')
  },
  {
    path: 'sign-up',
    name: prop + '.sign_up',
    meta: { auth: false },
    component: () => import('../pages/Sign-up.vue')
  }
]

const routes = [
  {
    path: '/auth',
    name: 'external_user',
    component: () => import('@/layouts/auth-layouts/ExternalUserAuthLayout.vue'),
    meta: { auth: false },
    children: childRoute('external_user')
  }
]

export default routes
