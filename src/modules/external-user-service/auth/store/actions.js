export default {
  setUserTypeList ({ commit }, payload) {
    commit('setUserTypeList', payload)
  },
  addUser ({ commit }, payload) {
    commit('addUser', payload)
  }
}
