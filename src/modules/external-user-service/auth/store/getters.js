export default {
  userTypeList: state => state.userTypeList,
  userFind: (state) => (id) => state.users.find(user => user.id === id)
}
