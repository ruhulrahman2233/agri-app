import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'
import custom from '../../../../store/custom'
import Custom from './custom'

const state = {
    commonObj: {
        divisionList: [],
        districtList: [],
        upazilaList: [],
        unionList: [],
        pauroshobaList: [],
        wardList: [],
        cityCorporationList: [],
        fiscalYearList: [],
        fertilizerTypeList: [],
        fertilizerNameList: [],
        allocationTypeList: [],
        monthList: Custom.monthList
    },
    static: {
        perPage: 10,
        dateFormat: 'DD/MM/YYYY',
        fiscaleYear: 'YYYY-YYYY',
        timeFormat: 'h:m',
        loading: false,
        listReload: false,
        hasDropdownLoaded: false
    },
    warehouseInfoList: [],
    regionList: [],
    regionDistrictList: [],
    affidavitList: [],
    orgList: [],
    schemeTypeList: [],
    subSchemeTypeList: [],
    waterTestingParameterList: [],
    orgComponentList: [],
    schemeApplicationLists: null,
    pumpOptApplicationLists: null,
    smartCardApplicationLists: null,
    waterTestingRequests: [],
    farmerBasicInfo: null,
    ginnerGrowerDetails: null,
    reissueStatus: 1,
    pumpList: [],
    pumpTypeList: [],
    warehouse: {
        hasDropdownLoaded: false,
        perPage: 10,
        dateFormat: 'dd/mm/YYYY',
        timeFormat: 'h:m',
        loading: false,
        listReload: false,
        commodityGroupList: [],
        commodityNameList: [],
        fiscalYearList: [],
        regionList: [],
        warehouseDesignationList: [],
        warehouseTypeList: [],
        warehouseInfoList: [],
        warehouseLevelList: [],
        marketInfoList: []
    },
    farmerBasicInfoStatus: [
        {
            value: 1,
            text: 'Draft',
            text_bn: 'ড্রাফট'
        },
        {
            value: 2,
            text: 'Saved',
            text_bn: 'সংরক্ষিত'
        }
    ],
    warehouseFarmerProfiles: [],
    ratinglists: [],
    genderList: custom.genderListEx,
    farmerWareInfo: null,
    reissueData: null,
    schemeFarmerList: [],
    schemeAppAddedFarmer: false,
    circleAreaList: [],
    pumpOptDetails: {},
    incentiveGrant: {
        hasDropdownLoaded: false,
        perPage: 10,
        dateFormat: 'dd/mm/YYYY',
        timeFormat: 'h:m',
        loading: false,
        listReload: false,
        appGeneralInfoID: null
    }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
