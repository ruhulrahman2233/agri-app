import actions from './actions'
import getters from './getters'
import { mutations } from './mutations'

const state = {
  isLoggedIn: false,
  userTypeList: [
    { user_type_id: 1, value: 1, user_type_name: 'Irrigation Farmer', user_type_name_bn: 'সেচ কৃষক', text: 'Irrigation Farmer' },
    { user_type_id: 2, value: 2, user_type_name: 'Warehouse Farmer', user_type_name_bn: 'গুদাম কৃষক', text: 'Warehouse Farmer' },
    { user_type_id: 7, value: 7, user_type_name: 'Grant Applicant', user_type_name_bn: 'অনুদান আবেদনকারী', text: 'Grant Applicant' }
  ],
  authUser: null,
  questions: [],
  authUserRoles: [],
  activeRoleId: 0,
  authorizedComponents: [],
  activeComponentId: 0,
  activeMenus: [],
  authorizedURIs: ['/home'],
  unauthorizedAccessCounter: 0
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
