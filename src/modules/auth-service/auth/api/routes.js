// Authentication and authorization
export const componentsByRoleApi = '/auth/components-by-role'
export const componentsOfSuperAdminApi = '/access-control/components-from-menu'
export const sidebarMenusApi = '/access-control/sidebar-menus'
// User data
export const userTypeListApi = '/external/user/user-type-list'
export const usernameCheckApi = '/external/user/check-username'
export const signUp = '/external/user/sign-up'
export const userDataApi = '/user/detail'
export const otpVerify = '/external/user/otp-verify'
export const resendOtp = '/external/user/otp-resend'
export const authUserApi = '/auth-user'
export const authUserRolesApi = '/auth/user-roles'
// Question
export const secretQuestionlist = '/secret-question/list'
export const secretQuestionStore = '/secret-question/store'
export const secretQuestionUpdate = '/secret-question/update'
export const secretQuestionToggleStatus = '/secret-question/toggle-status'
export const secretQuestionDestroy = '/secret-question/delete'
// Security
export const changePassword = '/user/change-password'
// Warehouse E-registration
export const warehouseSignUp = '/warehouseUser-registration/store'
