const authSignupRoutes = (prop) => [
  {
    path: 'sign-up',
    name: prop + '.sign_up',
    meta: { auth: false },
    component: () => import('../pages/Sign-up-old.vue')
  },
  {
    path: 'role-select',
    name: prop + '.role_select',
    meta: { auth: false },
    component: () => import('../pages/RoleSelect.vue')
  },
  {
    path: 'otp/:id',
    name: prop + '.reg-otp',
    meta: { auth: false },
    component: () => import('../pages/Otp.vue')
  },
  {
    path: 'dashboard',
    name: prop + '.dashboard',
    meta: { auth: true },
    component: () => import('../pages/Dashboard.vue')
  },
  {
    path: 'sign-up-success',
    name: prop + '.sign_up_success',
    meta: { auth: false },
    component: () => import('../pages/Sign-up-success.vue')
  },
  {
    path: 'switch-role',
    name: prop + '.switch_role',
    meta: { auth: false },
    component: () => import('../pages/SwitchRole.vue')
  }
]
const authChildRoutes = (prop) => [
  {
    path: 'login',
    name: prop + '.login',
    meta: { auth: false },
    component: () => import('../pages/Login.vue')
  },
  {
    path: 'e-registration',
    name: prop + '.e_registration',
    meta: { auth: false },
    component: () => import('../pages/E-Registration.vue')
  }
]

const passwordChangeChildRoutes = (prop) => [
  {
    path: 'password/change',
    name: prop + '.password_change',
    meta: { auth: true },
    component: () => import('../pages/security/ChangePassword.vue')
  }
]
const secertQuestionRoutes = (prop) => [
  {
    path: 'secret-question',
    name: prop + '.secret_question',
    meta: { auth: true },
    component: () => import('../pages/secret-question/List.vue')
  }
]

const routes = [
  {
    path: '/auth',
    name: 'auth',
    component: () => import('@/layouts/auth-layouts/AuthLayout'),
    meta: { auth: false },
    children: authChildRoutes('auth')
  },
  {
    path: '/auth',
    name: 'authSignUp',
    component: () => import('@/layouts/auth-layouts/AuthSignupLayout'),
    meta: { auth: false },
    children: authSignupRoutes('authSignUp')
  },
  {
    path: '/security',
    name: 'security',
    component: () => import('@/layouts/VerticleLayout'),
    meta: { auth: true },
    children: passwordChangeChildRoutes('security')
  },
  {
    path: '/common-service/user-management',
    name: 'common_service.secret_questions',
    component: () => import('@/layouts/VerticleLayout'),
    meta: { auth: true },
    children: secertQuestionRoutes('common_service.secret_questions')
  }
]

export default routes
