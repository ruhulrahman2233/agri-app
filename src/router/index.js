import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthRoutes from '../modules/auth-service/auth/routes'
import UserManagementRoutes from '../modules/auth-service/user-management/routes'
import organizationProfileRoutes from '../modules/common-config/org-profile/routes'
import organogramRoutes from '../modules/common-config/organogram/routes'
import complainRoutes from '../modules/common-config/complain/routes'
import notificationRoutes from '../modules/common-config/notification/routes'
import paymentManagementRoutes from '../modules/common-config/payment-management/routes'
import IrrigationSchemConfigurationRoutes from '../modules/irrigation-scheme-service/configuration/routes'
import IrrigationSchemeTaskRoutes from '../modules/irrigation-scheme-service/task/routes'
import IrrigationSchemeWaterTestingRoutes from '../modules/irrigation-scheme-service/water-testing/routes'
import IrrigationPumpInformationRoutes from '../modules/irrigation-scheme-service/pump-information/routes'
import ExternalUserIrrigationRoutes from '../modules/external-user-service/irrigation/routes'
import pumpInstallation from '../modules/irrigation-scheme-service/pump-installation/routes'
import pumpMaintenance from '../modules/irrigation-scheme-service/pump-maintenance/routes'
import farmerOperatorPanel from '../modules/irrigation-scheme-service/farmer-operator-panel/routes'
import documentRoutes from '../modules/common-config/document/routes'
import dataArchiveRoutes from '../modules/common-config/data-archive/routes'
import WarehouseConfigRoutes from '../modules/warehouse-service/configuration/routes'
import WarehouseInfoRoutes from '../modules/warehouse-service/information/routes'
import WarehouseServiceRoutes from '../modules/warehouse-service/ware-service/routes'
import WarehouseReportRoutes from '../modules/warehouse-service/report/routes'
import WarehouseServicePerformanceRoutes from '../modules/warehouse-service/service-performance/routes'
import WarehouseInfoServiceRoutes from '../modules/warehouse-service/info-service/routes'
import WarehouseWarehouseManagementRoutes from '../modules/warehouse-service/warehouse-management/routes'
import CommitteeRoutes from '../modules/common-config/committee/routes'
import cardPaymentRoutes from '../modules/irrigation-scheme-service/card-payment/routes'
import infoServiceManagementRoutes from '../modules/common-config/info-service-management/routes'
import seedsConfigRoutes from '../modules/seeds-fertilizer-service/seeds/configuration/routes'
import fertilizerConfigRoutes from '../modules/seeds-fertilizer-service/fertilizer/configuration/routes'
import fertilizerProcurementRoutes from '../modules/seeds-fertilizer-service/fertilizer/procurement/routes'
import seedsSeedsRoutes from '../modules/seeds-fertilizer-service/seeds/seeds-seeds/routes'
import seedsApplicationAllocationRoutes from '../modules/seeds-fertilizer-service/seeds/application-allocation/routes'
import seedsReportRoutes from '../modules/seeds-fertilizer-service/seeds/seeds-reports/routes'
import fertilizerMovementRoutes from '../modules/seeds-fertilizer-service/fertilizer/movement/routes'
import fertilizerRoutes from '../modules/seeds-fertilizer-service/fertilizer/fertilizer-reports/routes'
import fertilizerSalesRoutes from '../modules/seeds-fertilizer-service/fertilizer/sales/routes'
import fertilizerDealerManagementRoutes from '../modules/seeds-fertilizer-service/fertilizer/dealer-management/routes'
import agriMarketingMasterRoutes from '../modules/agri-marketing/master/routes'
import agriMarketingCottonConfigRoutes from '../modules/agri-marketing/cotton/configuration/routes'
import agriMarketingCropPriceInfoConfigRoutes from '../modules/agri-marketing/crop-price-info/configuration/routes'
import agriMarketingCottonGinnerGrowerRoutes from '../modules/agri-marketing/cotton/ginner-grower/routes'
import agriMarketingCottonReportsRoutes from '../modules/agri-marketing/cotton/reports/routes'
import agriMarketingEPustiConfigRoutes from '../modules/agri-marketing/e-pusti/configuration/routes'
import agriMarketingEPustiBirtanRoutes from '../modules/agri-marketing/e-pusti/birtan/routes'
import agriMarketingPustiMappingRoutes from '../modules/agri-marketing/pusti-mapping/routes'
import agriMarketingMarketLinkageConfigRoutes from '../modules/agri-marketing/market-linkage/config/routes'
import agriMarketingMarketLinkageLinkageRoutes from '../modules/agri-marketing/market-linkage/linkage/routes'
import agriMarketingMarketLinkageReportRoutes from '../modules/agri-marketing/market-linkage/report/routes'
import agriMarketingCropPriceInfoPriceInformationRoutes from '../modules/agri-marketing/crop-price-info/price-information/routes'
import agriMarketingCropPriceInfoReportRoutes from '../modules/agri-marketing/crop-price-info/reports/routes'
import seedsFerGermPlasmConfigurationRoutes from '../modules/seeds-fertilizer-service/germplasm/configuration/routes'
import seedsFerReportRoutes from '../modules/seeds-fertilizer-service/germplasm/report/routes'
import seedsFerGermPlasmGermPlasmRoutes from '../modules/seeds-fertilizer-service/germplasm/germplasm/routes'
// Incentive Grant Service Started from here
import incentiveGrantMasterRoutes from '../modules/incentive-grant-service/master/routes'
import incentiveGrantBarcConfigRoutes from '../modules/incentive-grant-service/barc/configuration/routes'
import incentiveGrantBSRIConfigRoutes from '../modules/incentive-grant-service/bsri/configuration/routes'
import incentiveGrantDAEConfigRoutes from '../modules/incentive-grant-service/dae/configuration/routes'
import trainingElearningConfigRoutes from '../modules/training-e-learning-service/configuration/routes'

/* Layouts */
const AuthSignUpLayout = () => import('../layouts/auth-layouts/AuthSignupLayout.vue')
const NotFoundPage = () => import('../views/Pages/ErrorPage.vue')

Vue.use(VueRouter)

const childRoutes = (prop) => [
  {
    path: '',
    // name: prop + '.home-2',
    // meta: { auth: true, name: 'Home 2' },
    // component: Dashboard2,
    redirect: { name: 'authSignUp.dashboard' }
  },
  {
    path: '/home',
    // name: prop + '.home-2',
    // meta: { auth: true, name: 'Home 2' },
    // component: Dashboard2,
    redirect: { name: 'authSignUp.dashboard' }
  }
]

const defaultRoutes = [
  {
    path: '',
    name: 'dashboard',
    component: AuthSignUpLayout,
    meta: { auth: true },
    children: childRoutes('dashboard')
  },
  {
    path: '/seeds-fertilizer-service',
    name: 'seeds_fertilizer_service',
    component: () => import('@/layouts/SeedsFertilizerLayout'),
    meta: { auth: true },
    children: [
      {
        path: 'dashboard',
        name: 'seeds_fertilizer_service.dashboard',
        component: () => import('@/views/services-main-dashboard/SeedsFertilizer.vue')
      }
    ]
  },
  {
    path: '/training-e-learning-service',
    name: 'training_e_learning_service',
    component: () => import('@/layouts/TrainingElearningLayout'),
    meta: { auth: true },
    children: [
      {
        path: 'dashboard',
        name: 'training_e_learning_service.dashboard',
        component: () => import('@/views/services-main-dashboard/trainingElearning.vue')
      }
    ]
  },
  {
      path: '*',
      meta: { auth: false },
      component: NotFoundPage
  }
]

const routes = [
  ...defaultRoutes,
  ...AuthRoutes,
  ...UserManagementRoutes,
  ...organizationProfileRoutes,
  ...organogramRoutes,
  ...complainRoutes,
  ...notificationRoutes,
  ...paymentManagementRoutes,
  ...IrrigationSchemConfigurationRoutes,
  ...IrrigationSchemeTaskRoutes,
  ...IrrigationSchemeWaterTestingRoutes,
  ...ExternalUserIrrigationRoutes,
  ...pumpInstallation,
  ...farmerOperatorPanel,
  ...documentRoutes,
  ...dataArchiveRoutes,
  ...WarehouseConfigRoutes,
  ...CommitteeRoutes,
  ...IrrigationPumpInformationRoutes,
  ...WarehouseInfoRoutes,
  ...WarehouseServiceRoutes,
  ...cardPaymentRoutes,
  ...WarehouseReportRoutes,
  ...pumpMaintenance,
  ...WarehouseServicePerformanceRoutes,
  ...WarehouseInfoServiceRoutes,
  ...WarehouseWarehouseManagementRoutes,
  ...infoServiceManagementRoutes,
  ...seedsConfigRoutes,
  ...fertilizerConfigRoutes,
  ...fertilizerProcurementRoutes,
  ...seedsSeedsRoutes,
  ...seedsApplicationAllocationRoutes,
  ...seedsReportRoutes,
  ...fertilizerMovementRoutes,
  ...fertilizerRoutes,
  ...fertilizerSalesRoutes,
  ...fertilizerDealerManagementRoutes,
  ...agriMarketingMasterRoutes,
  ...agriMarketingCottonConfigRoutes,
  ...agriMarketingCottonReportsRoutes,
  ...agriMarketingEPustiConfigRoutes,
  ...agriMarketingEPustiBirtanRoutes,
  ...agriMarketingCropPriceInfoConfigRoutes,
  ...agriMarketingCottonGinnerGrowerRoutes,
  ...agriMarketingPustiMappingRoutes,
  ...agriMarketingMarketLinkageConfigRoutes,
  ...agriMarketingMarketLinkageLinkageRoutes,
  ...agriMarketingMarketLinkageReportRoutes,
  ...agriMarketingCropPriceInfoPriceInformationRoutes,
  ...agriMarketingCropPriceInfoReportRoutes,
  ...seedsFerGermPlasmConfigurationRoutes,
  ...seedsFerReportRoutes,
  ...seedsFerGermPlasmGermPlasmRoutes,
  ...incentiveGrantMasterRoutes,
  ...incentiveGrantBarcConfigRoutes,
  ...incentiveGrantBSRIConfigRoutes,
  ...incentiveGrantDAEConfigRoutes,
  ...trainingElearningConfigRoutes
]
const router = new VueRouter({
  mode: 'history',
  base: process.env.VUE_APP_BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth)) {
    const loggedIn = localStorage.getItem('access_token')
    const authUser = localStorage.getItem('user')
    if (loggedIn && authUser) {
        return next()
    }

    return next({ path: '/auth/login' })
  }

  next()
})

export default router
