import RestApi, { agriMarketingServiceBaseUrl } from '../config/api_config'

export default {
  computed: {
    hasDropdownLoadedAgriMarketing () {
      return this.$store.state.agriMarketing.commonObj.hasDropdownLoaded
    }
  },
  watch: {
    hasDropdownLoadedAgriMarketing: function (newValue) {
      if (!newValue) {
        this.loadDropdownCommonConfig()
        this.loadDropdownDam()
      }
    }
  },
  created () {
    const hasDropdownLoadedAgriMarketing = this.$store.state.agriMarketing.hasDropdownLoaded
    if (!hasDropdownLoadedAgriMarketing || window.performance) {
    }
    this.loadDropdownCommonConfig()
    this.loadDropdownDam()
  },
  methods: {
    loadDropdownCommonConfig () {
      RestApi.getData(agriMarketingServiceBaseUrl, 'common-dropdowns', null).then(response => {
        if (response.success) {
          this.$store.dispatch('agriMarketing/mutateAgriMarketingProperties', {
            hasDropdownLoaded: true,
            regionList: response.data.regionsList,
            zoneList: response.data.zonesList,
            unitList: response.data.unitList,
            hatList: response.data.hatsList,
            seasonList: response.data.seasonsList,
            cottonVaritiesList: response.data.cottonVaritiesList,
            cottonNameList: response.data.cottonNamesList,
            commodityGroupList: response.data.commodityGroupList,
            commoditySubGroupList: response.data.commoditySubGroupList,
            commodityNameList: response.data.commodityNameList,
            campaignNameList: response.data.campaignNameList,
            divisionalOfficeList: response.data.divisionalOfficeList,
            commodityTypeList: response.data.commodityTypeList,
            marketList: response.data.marketList,
            alertPercentageList: response.data.alertPercentageList,
            ginnerGrowerList: response.data.ginnerGrowerList,
            leaseYearList: response.data.leaseYearList,
            measurementUnitList: response.data.measurementUnitList,
            infrastructureList: response.data.infrastructureList,
            communicationLinkageList: response.data.communicationLinkageList,
            designationOfProductList: response.data.designationOfProductList,
            vehicleList: response.data.vehicleList
          })
          this.$store.dispatch('agriMarketing/changeAgriMarketingDropdown', { value: this.$i18n.locale })
        }
      })
    },
    loadDropdownDam () {
      RestApi.getData(agriMarketingServiceBaseUrl, 'common-data-dam', null).then(response => {
        if (response.success) {
          this.$store.commit('agriMarketing/mutateDataDam', {
            hasDropdownLoaded: true,
            divisionList: response.data.divisionList,
            districtList: response.data.districtList,
            upazilaList: response.data.upazilaList
          })
          this.$store.dispatch('agriMarketing/mutateDataDamLocale', { value: this.$i18n.locale })
        }
      })
    }
  }
}
