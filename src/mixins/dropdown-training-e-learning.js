import RestApi, { trainingElearningServiceBaseUrl } from '../config/api_config'

export default {
  computed: {
    hasDropdownLoadedTrainingElearningService () {
      return this.$store.state.TrainingElearning.commonObj.hasDropdownLoaded
    }
  },
  watch: {
    hasDropdownLoadedTrainingElearningService: function (newValue) {
      if (!newValue) {
        this.loadDropdownCommonConfig()
      }
    }
  },
  created () {
    const hasDropdownLoadedTrainingElearningService = this.$store.state.TrainingElearning.hasDropdownLoaded
    if (!hasDropdownLoadedTrainingElearningService || window.performance) {
    }
    this.loadDropdownCommonConfig()
  },
  methods: {
    loadDropdownCommonConfig () {
      RestApi.getData(trainingElearningServiceBaseUrl, 'common-dropdowns', null).then(response => {
        if (response.success) {
          this.$store.commit('TrainingElearning/mutateTrainingElearningCommonProperties', {
            hasDropdownLoaded: true,
            trainingTypeList: response.data.trainingTypeList,
            trainingCategoryList: response.data.trainingCategoryList,
            trainingTitleList: response.data.TrainingTitleList,
            roomTypeList: response.data.RoomTypeList,
            accomodationSetupList: response.data.accomodationSetupList,
            courseDocumentSetupList: response.data.courseDocumentSetupList
          })
          this.$store.dispatch('TrainingElearning/localizeCommonDropdownTrainingElearningService', { value: this.$i18n.locale })
        }
      })
    }
  }
}
