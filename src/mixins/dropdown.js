import RestApi, { commonServiceBaseUrl } from '../config/api_config'
import { mapGetters } from 'vuex'

export default {
  data () {
    return {
      mixinComponentId: 0,
      isMinistryUser: false
    }
  },
  computed: {
    hasAllDropdownLoaded () {
      return this.$store.state.commonObj.hasDropdownLoaded
    },
    mixinCurrentLocale () {
      return this.$i18n.locale
    },
    ...mapGetters({
      orgList: 'orgList',
      orgComponentList: 'orgComponentList',
      authUser: 'Auth/authUser',
      activeRoleId: 'Auth/getActiveRole'
    })
  },
  watch: {
    hasAllDropdownLoaded: function (newValue) {
      if (!newValue) {
        this.loadCommonDropdown()
      }
    },
    $route () {
      this.checkMenuAccessPermission()
    },
    mixinCurrentLocale: function (newValue, oldValue) {
      if (newValue !== oldValue) {
        this.changeLangActiveMenus(newValue)
      }
    }
  },
  created () {
    this.setIsMinistryUser()
    // Loading common dropdowns
    const hasDropdownLoaded = this.$store.state.commonObj.hasDropdownLoaded

    if (!hasDropdownLoaded || window.performance) {
      this.setOrgAndOrgComponentList()
      this.loadCommonDropdown()
    }
    this.checkMenuAccessPermission()
  },
  methods: {
    setOrgAndOrgComponentList () {
      RestApi.getData(commonServiceBaseUrl, 'common/org-and-org-component-list').then(response => {
        if (response.success === true) {
          const tmpData = response.data.orgList.map(item => {
            const tmp = this.$i18n.locale === 'en' ? { text: item.text_en } : { text: item.text_bn }
            return Object.assign({}, item, tmp)
          })
          this.$store.commit('setOrgAndOrgComponentList', {
            orgList: tmpData,
            orgComponentList: response.data.orgComponentList
          })

          this.mixinSetOrganizationProfileList()
        }
      })
    },
    mixinSetOrganizationProfileList () {
      const orgComponents = this.getOrgAdminData()
      const orgCompList = orgComponents.map(item => {
        const orgObj = this.orgList.find(org => org.value === item.org_id)

        return this.$i18n.locale === 'en' ? Object.assign({}, orgObj, { text: orgObj.text_en }) : Object.assign({}, orgObj, { text: orgObj.text_bn })
      })

      this.$store.commit('mutateCommonProperties', { organizationProfileList: this.getOrgByUserType(orgCompList) })
    },
    loadCommonDropdown () {
      RestApi.getData(commonServiceBaseUrl, 'common-dropdowns', null).then(response => {
        if (response.success) {
          const fieldOfficerFilterData = this.getFieldOfficerFilterData(response.data)
          this.$store.commit('mutateCommonProperties', {
            hasDropdownLoaded: true,
            divisionList: fieldOfficerFilterData.divisionList,
            districtList: fieldOfficerFilterData.districtList,
            upazilaList: fieldOfficerFilterData.upazilaList,
            unionList: fieldOfficerFilterData.unionList,
            officeTypeList: response.data.officeTypeList,
            officeList: response.data.officeList,
            designationList: response.data.designationList,
            gradeList: response.data.gradeList,
            bankObj: {
                bankList: response.data.bankList,
                branchList: response.data.branchList
            },
            fiscalYearList: this.fiscalYearBnAdd(response.data.fiscalYearList)
          })
          this.$store.dispatch('changeCommonDropdown', { value: this.$i18n.locale })
        }
      })
    },
    fiscalYearBnAdd (data) {
      const bnAdded = data.map(item => {
        const fiscalYearData = {}
        fiscalYearData.text_bn = this.$i18n.n(item.text_bn.split('-')[0], { useGrouping: false }) + '-' + this.$i18n.n(item.text_bn.split('-')[1], { useGrouping: false })
        return Object.assign({}, item, fiscalYearData)
      })
      return bnAdded
    },
    checkMenuAccessPermission () {
      const authorizedURIs = this.$store.state.Auth.authorizedURIs
      const unauthorizedAccessCounter = this.$store.state.Auth.unauthorizedAccessCounter
      const accessUri = this.$route.matched[1].path
      if (authorizedURIs.indexOf(accessUri) === -1 && unauthorizedAccessCounter > 2) {
        this.$store.dispatch('Auth/setUnauthorizedAccessCounter', 0)
        this.$toast.error({
          title: 'Error',
          message: 'Sorry! Unauthorized access.' + accessUri
        })
        this.$router.push('/auth/login')
      } else if (authorizedURIs.indexOf(accessUri) === -1) {
        this.$store.dispatch('Auth/setUnauthorizedAccessCounter', (unauthorizedAccessCounter + 1))
        this.$toast.error({
          title: 'Error',
          message: 'Sorry! Unauthorized access.' + accessUri
        })
        this.$router.go(-1)
      }
    },
    changeLangActiveMenus (lang) {
      const activeMenus = this.$store.state.Auth.activeMenus
      const tmpMenus = activeMenus.map(item => {
        if (Object.prototype.hasOwnProperty.call(item, 'children')) {
          return Object.assign({}, item, {
            name: lang === 'en' ? item.title : item.title_bn,
            children: this.getChildMenus(item.children, lang)
          })
        } else {
          return Object.assign({}, item, {
            name: lang === 'en' ? item.title : item.title_bn
          })
        }
      })
      this.$store.dispatch('Auth/setActiveMenus', tmpMenus)
    },
    getChildMenus (menuItems, lang) {
      return menuItems.map(item => {
        if (Object.prototype.hasOwnProperty.call(item, 'children')) {
          return Object.assign({}, item, {
            name: lang === 'en' ? item.title : item.title_bn,
            children: this.getChildMenus(item.children, lang)
          })
        } else {
          return Object.assign({}, item, {
            name: lang === 'en' ? item.title : item.title_bn
          })
        }
      })
    },
    getOrgAdminData () {
      if (this.authUser.is_org_admin) {
        return this.orgComponentList.filter(item => (item.component_id === this.mixinComponentId) && (item.org_id === this.authUser.org_id))
      }
      return this.orgComponentList.filter(item => item.component_id === this.mixinComponentId)
    },
    getOrgByUserType (orgList) {
      if (this.activeRoleId === 1 || this.isMinistryUser) {
        return orgList
      } else {
        return orgList.filter(item => item.value === parseInt(this.authUser.org_id))
      }
    },
    getFieldOfficerFilterData (data) {
      const filteredData = {
        divisionList: data.divisionList,
        districtList: data.districtList,
        upazilaList: data.upazilaList,
        unionList: data.unionList
      }
      if (this.activeRoleId === 1 || this.isMinistryUser) {
        return filteredData
      }
      if (this.authUser.office_detail.division_id && this.authUser.is_org_admin === 0) {
        filteredData.divisionList = data.divisionList.filter(item => item.value === parseInt(this.authUser.office_detail.division_id))
      }
      if (this.authUser.office_detail.district_id && this.authUser.is_org_admin === 0) {
        filteredData.districtList = data.districtList.filter(item => item.value === parseInt(this.authUser.office_detail.district_id))
      }
      if (this.authUser.office_detail.upazilla_id && this.authUser.is_org_admin === 0) {
        filteredData.upazilaList = data.upazilaList.filter(item => item.value === parseInt(this.authUser.office_detail.upazilla_id))
      }
      if (this.authUser.office_detail.upazilla_id && this.authUser.is_org_admin === 0) {
        filteredData.unionList = data.unionList.filter(item => item.value === parseInt(this.authUser.office_detail.union_id))
      }
      return filteredData
    },
    setIsMinistryUser () {
      this.isMinistryUser = true

      if (this.activeRoleId === 1) {
        this.isMinistryUser = false
      }
    }
  }
}
