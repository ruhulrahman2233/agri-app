import RestApi, { incentiveGrantServiceBaseUrl } from '../config/api_config'

export default {
  computed: {
    hasDropdownLoadedIncentiveGrantService () {
      return this.$store.state.incentiveGrant.hasDropdownLoaded
    }
  },
  watch: {
    hasDropdownLoadedIncentiveGrantService: function (newValue) {
      if (!newValue) {
        this.loadDropdownCommonConfig()
      }
    }
  },
  created () {
    const hasDropdownLoadedIncentiveGrantService = this.$store.state.incentiveGrant.hasDropdownLoaded
    if (!hasDropdownLoadedIncentiveGrantService || window.performance) {
    }
    this.loadDropdownCommonConfig()
  },
  methods: {
    loadDropdownCommonConfig () {
      RestApi.getData(incentiveGrantServiceBaseUrl, 'common-dropdowns', null).then(response => {
        if (response.success) {
          this.$store.commit('incentiveGrant/mutateIncentiveGrantCommonProperties', {
            hasDropdownLoaded: true,
            enlistedUniversityList: response.data.enlistedUniversityList,
            educationLevelList: response.data.educationLevelList,
            grantList: response.data.grantList,
            itemList: response.data.itemList,
            millTypeList: response.data.millTypeList,
            barcMsApplicationNameList: response.data.barcMsApplicationNameList,
            cultivationMethodList: response.data.cultivationMethodList,
            cropList: response.data.cropList,
            seasonSetupList: response.data.seasonSetupList,
            millInfoList: response.data.millInfoList,
            subsidyTypeList: response.data.subsidyTypeList,
            subsidyList: response.data.subsidyList,
            projectList: response.data.projectList,
            regionInfoList: response.data.regionInfoList,
            applicationList: response.data.applicationList,
            villageList: response.data.villageList,
            agMaterialTypeList: response.data.agMaterialTypeList,
            agMaterialList: response.data.agMaterialList,
            publicationTypeList: response.data.publicationTypeList,
            circularInfoList: response.data.circularInfoList,
            narseInstituteList: response.data.narseInstituteList,
            committeeList: response.data.committeeList,
            fundSubHeadList: response.data.fundSubHeadList,
            committeeRollList: response.data.committeeRollList
          })
          this.$store.dispatch('incentiveGrant/localizeIncentiveGrantDropdown', { value: this.$i18n.locale })
        }
      })
    }
  }
}
