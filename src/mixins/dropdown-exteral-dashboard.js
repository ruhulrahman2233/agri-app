import RestApi, {
    authServiceBaseUrl,
    commonServiceBaseUrl,
    irriSchemeServiceBaseUrl,
    warehouseServiceBaseUrl,
    agriMarketingServiceBaseUrl,
    seedFertilizerServiceBaseUrl,
    incentiveGrantServiceBaseUrl
} from '../config/api_config'
import { mapGetters } from 'vuex'

export default {
    computed: {
        ginnerGrowerUser () {
            return this.$store.state.Auth.authUser.user_type_id === 3 || this.$store.state.Auth.authUser.user_type_id === 4
        },
        isWareHouseUser () {
            return this.$store.state.Auth.authUser.user_type_id === 2
        },
        isIncentiveGrantuser () {
            return this.$store.state.Auth.authUser.user_type_id === 7
        },
        mixinComponent () {
          return this.isWareHouseUser ? 4 : 9
        },
        hasAllDropdownLoaded () {
            return this.$store.state.ExternalUserIrrigation.static.hasDropdownLoaded
        },
        ...mapGetters({
            orgList: 'ExternalUserIrrigation/orgList',
            orgComponentList: 'ExternalUserIrrigation/orgComponentList'
        })
    },
    watch: {
        hasAllDropdownLoaded: function (newValue) {
            if (!newValue) {
                this.loadCommonDropdown()
            }
        }
    },
    created () {
        // Loading common dropdowns
        const hasDropdownLoaded = this.$store.state.ExternalUserIrrigation.static.hasDropdownLoaded
        if (!hasDropdownLoaded || window.performance) {
            this.setOrgAndOrgComponentList()
            this.loadCommonDropdown()
            this.setAffidavitList()
            this.setPumpTypeList()
            this.setUserDetails()
            this.setSchemeTypeList()
            this.setSubSchemeTypeList()
            this.setCircleAreaList()
            this.setWaterTestingParameter()
            this.getOperatorInfo()
        }

        // if (this.hasAllDropdownLoaded) {
            if (this.isWareHouseUser) {
                this.loadDropdownWarehouse()
                this.getRegionList()
                this.getRegionDistrictList()
            }
            if (this.ginnerGrowerUser) {
                this.loadDropdownGinnerGrower()
            }
            if (this.isIncentiveGrantuser) {
                this.loadIncentiveGrantDropdown()
            }
        // }
    },
    methods: {
        getRegionList () {
            RestApi.getData(warehouseServiceBaseUrl, '/master-region-info/list-all').then(response => {
                if (response.success) {
                    const regionList = response.data.map(obj => {
                        if (this.$i18n.locale === 'bn') {
                            return { value: obj.id, text: obj.region_name_bn }
                        } else {
                            return { value: obj.id, text: obj.region_name }
                        }
                    })
                    this.$store.commit('ExternalUserIrrigation/setRegionList', regionList)
                }
            })
        },
        getRegionDistrictList () {
            RestApi.getData(warehouseServiceBaseUrl, '/master-warehouse-level/region-district-list').then(response => {
                if (response) {
                    const districts = this.$store.state.ExternalUserIrrigation.commonObj.districtList
                    const regions = this.$store.state.ExternalUserIrrigation.regionList
                    const isEng = this.$i18n.locale === 'en'
                    const regionDistrict = response.map((item) => {
                        const district = districts.find(district => district.value === item.district_id)
                        let formattedDistrict = {}
                        // if (district && district.status === 0) {
                        if (district) {
                            formattedDistrict = Object.assign(isEng ? { district_text: district.text_en }
                            : { text: district.text_bn }, { district_id: district.value, region_id: item.region_id })
                        }
                        const region = regions.find(region => region.value === item.region_id)
                        return Object.assign({}, { region_text: region.text }, formattedDistrict)
                    })
                    this.$store.commit('ExternalUserIrrigation/setRegionDistrictList', regionDistrict)
                }
            })
        },
        loadDropdownWarehouse () {
            RestApi.getData(warehouseServiceBaseUrl, 'warehouse-dropdown-list-all', null).then(response => {
                    if (response.status_code === 200) {
                        this.$store.commit('ExternalUserIrrigation/mutateWarehouseProperties', {
                            hasDropdownLoaded: true,
                            commodityGroupList: response.data.commodityGroupList,
                            commodityNameList: response.data.commodityNameList,
                            regionList: response.data.regionList,
                            fiscalYearList: this.fiscalYearBnAdd(response.data.fiscalYearList),
                            warehouseDesignationList: response.data.warehouseDesignationList,
                            warehouseTypeList: response.data.warehouseTypeList,
                            warehouseInfoList: response.data.warehouseInfoList,
                            warehouseLevelList: response.data.warehouseLevelList,
                            marketInfoList: response.data.marketInfoList
                        })
                        this.$store.dispatch('ExternalUserIrrigation/changeWarehouseDropdown', { value: this.$i18n.locale })
                    }
                })
        },
        fiscalYearBnAdd (data) {
            const bnAdded = data.map(item => {
                const fiscalYearData = {}
                fiscalYearData.text_bn = this.$i18n.n(item.text_bn.split('-')[0], { useGrouping: false }) + '-' + this.$i18n.n(item.text_bn.split('-')[1], { useGrouping: false })
                return Object.assign({}, item, fiscalYearData)
            })
            return bnAdded
        },
        loadDropdownGinnerGrower () {
            RestApi.getData(agriMarketingServiceBaseUrl, 'common-dropdowns', null).then(response => {
                    if (response.status_code === 200) {
                        this.$store.commit('ExternalUserIrrigation/mutateAgriMarketingProperties', {
                            hasDropdownLoaded: true,
                            regionList: response.data.regionsList,
                            zoneList: response.data.zonesList,
                            unitList: response.data.unitsList,
                            hatList: response.data.hatsList,
                            seasonList: response.data.seasonsList,
                            cottonVaritiesList: response.data.cottonVaritiesList,
                            cottonNameList: response.data.cottonNamesList
                        })
                        this.$store.dispatch('ExternalUserIrrigation/changeAgriMarketingDropdown', { value: this.$i18n.locale })
                    }
                })
        },
        setSchemeTypeList () {
            RestApi.getData(irriSchemeServiceBaseUrl, '/scheme-type/list', { no_pagination: true, status: 0 }).then((response) => {
                if (response.success) {
                    this.schemeTypeListData = response.data
                    this.$store.commit('ExternalUserIrrigation/setSchemeTypeList', response.data)
                }
            })
        },
        setSubSchemeTypeList () {
            RestApi.getData(irriSchemeServiceBaseUrl, '/sub-scheme-type/list', { no_pagination: true, status: 0 }).then((response) => {
                if (response.success) {
                    this.subSchemeTypeListData = response.data
                    this.$store.commit('ExternalUserIrrigation/setSubSchemeTypeList', response.data)
                }
            })
        },
        setUserDetails () {
            RestApi.getData(authServiceBaseUrl, '/auth-user').then((response) => {
                try {
                    if (response.success) {
                        this.$store.commit('ExternalUserAuth/setFarmerDetails', response.data)
                    }
                } catch (e) {
                    alert('You were logged out!')
                }
            })
        },
        setAffidavitList () {
                RestApi.getData(irriSchemeServiceBaseUrl, '/external/affidavit-list').then(response => {
                if (response.success === true) {
                    const tmpData = response.data.map(item => {
                        const tmp = this.$i18n.locale === 'en' ? { text: item.title } : { text: item.title_bn }
                        return Object.assign({}, item, tmp)
                    })
                    this.$store.commit('ExternalUserIrrigation/setAffidavitList', tmpData)
                }
            })
        },
        setPumpTypeList () {
                RestApi.getData(irriSchemeServiceBaseUrl, '/external/pump-type-list').then(response => {
                if (response.success === true) {
                    const tmpData = response.data.map(item => {
                        const tmp = this.$i18n.locale === 'en' ? { text: item.title } : { text: item.title_bn }
                        return Object.assign({}, item, tmp)
                    })
                    this.$store.commit('ExternalUserIrrigation/setPumpTypeList', tmpData)
                }
            })
        },
        setCircleAreaList () {
                RestApi.getData(irriSchemeServiceBaseUrl, '/external/circle-area-list').then(response => {
                if (response.success === true) {
                    const tmpData = response.data.map(item => {
                        const tmp = this.$i18n.locale === 'en' ? { text: item.title } : { text: item.title_bn }
                        return Object.assign({}, item, tmp)
                    })
                    this.$store.commit('ExternalUserIrrigation/setCircleAreaList', tmpData)
                }
            })
        },
        setWaterTestingParameter () {
                RestApi.getData(irriSchemeServiceBaseUrl, '/external/testing-parameters').then(response => {
                if (response.success === true) {
                    const tmpData = response.data.map(item => {
                        const tmp = this.$i18n.locale === 'en' ? { text: item.text_en } : { text: item.text_bn }
                        return Object.assign({}, item, tmp)
                    })
                    this.$store.commit('ExternalUserIrrigation/setWaterTestingParameterList', tmpData)
                }
            })
        },
        setOrgAndOrgComponentList () {
          RestApi.getData(commonServiceBaseUrl, 'common/org-and-org-component-list').then(response => {
            if (response.success === true) {
              const tmpData = response.data.orgList.filter(item => {
                  if (item.status !== 1) {
                      const tmp = this.$i18n.locale === 'en' ? { text: item.text_en } : { text: item.text_bn }
                      return Object.assign({}, item, tmp)
                  }
              })
              this.$store.commit('ExternalUserIrrigation/setOrgAndOrgComponentList', {
                orgList: tmpData,
                orgComponentList: response.data.orgComponentList
              })

              this.mixinSetOrganizationProfileList(this.mixinComponent)
            }
          })
        },
        mixinSetOrganizationProfileList (mixinComponentId) {
          const orgComponents = this.orgComponentList.filter(item => item.component_id === mixinComponentId)
          const orgCompList = orgComponents.map(item => {
            const orgObj = this.orgList.find(org => org.value === item.org_id)

            return this.$i18n.locale === 'en' ? Object.assign({}, orgObj, { text: orgObj.text_en }) : Object.assign({}, orgObj, { text: orgObj.text_bn })
          })

          this.$store.commit('ExternalUserIrrigation/mutateExternalDropdown', { organizationProfileList: orgCompList })
        },
        loadCommonDropdown () {
          RestApi.getData(commonServiceBaseUrl, 'external-user-dropdowns', null).then(response => {
            if (response.success) {
                this.$store.commit('ExternalUserIrrigation/mutateExternalDropdown', {
                    hasDropdownLoaded: true,
                    divisionList: response.data.divisionList.filter(item => item.status !== 1),
                    districtList: response.data.districtList.filter(item => item.status !== 1),
                    upazilaList: response.data.upazilaList.filter(item => item.status !== 1),
                    unionList: response.data.unionList.filter(item => item.status !== 1),
                    pauroshobaList: response.data.pauroshobaList.filter(item => item.status !== 1),
                    wardList: response.data.wardList.filter(item => item.status !== 1),
                    cityCorporationList: response.data.cityCorporationList.filter(item => item.status !== 1),
                    fiscalYearList: this.fiscalYearBnAdd(response.data.fiscalYearList.filter(item => item.status !== 1))
                })
                this.$store.commit('ExternalUserIrrigation/localizeExternalDropdown', { value: this.$i18n.locale })
            }
          })

          RestApi.getData(seedFertilizerServiceBaseUrl, 'common-dropdowns', null).then(response => {
            if (response.success) {
                this.$store.commit('ExternalUserIrrigation/mutateExternalDropdown', {
                    hasDropdownLoaded: true,
                    fertilizerTypeList: response.data.fertilizerTypeList,
                    fertilizerNameList: response.data.fertilizerNameList,
                    allocationTypeList: response.data.allocationTypeList
              })
              this.$store.commit('ExternalUserIrrigation/localizeExternalDropdown', { value: this.$i18n.locale })
            }
          })
        },
        getOperatorInfo () {
            RestApi.getData(irriSchemeServiceBaseUrl, 'get-opt-info' + '/' + this.$store.state.Auth.authUser.id).then(response => {
                if (response.success === true) {
                    this.$store.dispatch('ExternalUserIrrigation/addPumpOptDetails', response.data)
                }
            })
        },
        loadIncentiveGrantDropdown () {
            RestApi.getData(incentiveGrantServiceBaseUrl, 'external-user-dropdown', null).then(response => {
                this.$store.dispatch('ExternalUserIrrigation/mutateIncentiveGrantProperties', {
                    hasDropdownLoaded: true,
                    circularInfoList: response.data.circularInfoList,
                    narseInstituteList: response.data.narseInstituteList,
                    educationLevelList: response.data.educationLevelList
                })
                this.$store.dispatch('ExternalUserIrrigation/changeIncentiveGrantDropdown', { value: this.$i18n.locale })
            })
        }
    }
}
